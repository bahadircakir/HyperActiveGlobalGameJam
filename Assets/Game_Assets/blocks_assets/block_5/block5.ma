//Maya ASCII 2023 scene
//Name: block5.ma
//Last modified: Sat, Feb 04, 2023 11:51:09 AM
//Codeset: UTF-8
requires maya "2023";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "5.2.1.1";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2023";
fileInfo "version" "2023";
fileInfo "cutIdentifier" "202211021031-847a9f9623";
fileInfo "osv" "Mac OS X 10.16";
fileInfo "UUID" "04919D4E-FA40-CC98-5512-12A26CAE2482";
createNode transform -s -n "persp";
	rename -uid "8156C96D-7540-CF3B-316E-D8898D0D05F6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -11.666677650788735 5.0700503904608336 6.9987606076257887 ;
	setAttr ".r" -type "double3" -19.538352729606348 -62.199999999999818 -3.4097832210047723e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "1841D248-3645-DF24-D231-FA91DEBCA81F";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 12.543994189614821;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "127C0318-CF4F-4E99-B1DC-659D1904BCF2";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "E968B0AD-954D-FAC4-B1D8-00B7CF02CB91";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "DEC404F1-2A4A-7756-7770-B99147DB40AD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "07CF99E6-5E4B-768B-299F-689CAF2A2722";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "38AB24B4-1742-D01E-8C21-18865EE9A198";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "32DBFA7D-B046-F66C-61B6-AF9D32E892B3";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "polySurface1";
	rename -uid "3FA64687-374C-A6B4-3E0E-9C8B359DA783";
	setAttr ".t" -type "double3" 2.495013263949061 0 2.0172390220344276 ;
createNode mesh -n "polySurfaceShape1" -p "polySurface1";
	rename -uid "F321FE7F-0847-6484-C227-A7A4DF8ECB3E";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:130]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 7 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 10 "f[21]" "f[32]" "f[59]" "f[75]" "f[89]" "f[101]" "f[111]" "f[119]" "f[125]" "f[129:130]";
	setAttr ".gtag[1].gtagnm" -type "string" "booleanIntersection";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 40 "e[2]" "e[5]" "e[8]" "e[11]" "e[14]" "e[17]" "e[20]" "e[23]" "e[26]" "e[29]" "e[32]" "e[35]" "e[38]" "e[41]" "e[44]" "e[47]" "e[50]" "e[53]" "e[56]" "e[59]" "e[61]" "e[63]" "e[66]" "e[69]" "e[72]" "e[75]" "e[78]" "e[81]" "e[84]" "e[87]" "e[90]" "e[97]" "e[99]" "e[102]" "e[105]" "e[108]" "e[111]" "e[114]" "e[116]" "e[119]";
	setAttr ".gtag[2].gtagnm" -type "string" "bottom";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 10 "f[10:19]" "f[33:41]" "f[50:57]" "f[67:73]" "f[82:87]" "f[95:99]" "f[106:109]" "f[115:117]" "f[122:123]" "f[127]";
	setAttr ".gtag[3].gtagnm" -type "string" "front";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[4].gtagnm" -type "string" "left";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 10 "f[20]" "f[31]" "f[58]" "f[74]" "f[88]" "f[100]" "f[110]" "f[118]" "f[124]" "f[128]";
	setAttr ".gtag[5].gtagnm" -type "string" "right";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[6].gtagnm" -type "string" "top";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 10 "f[0:9]" "f[22:30]" "f[42:49]" "f[60:66]" "f[76:81]" "f[90:94]" "f[102:105]" "f[112:114]" "f[120:121]" "f[126]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 212 ".uvst[0].uvsp[0:211]" -type "float2" 0.44308054 0.32499999
		 0.42500001 0.32500002 0.42500001 0.30691946 0.39308056 0.27500004 0.375 0.27500001
		 0.375 0.25691944 0.41808057 0.30000001 0.40000001 0.30000001 0.40000001 0.28191948
		 0.49308056 0.37500003 0.47500002 0.37500003 0.47500002 0.35691947 0.46808058 0.35000002
		 0.45000002 0.35000002 0.45000002 0.33191946 0.51808053 0.40000004 0.5 0.40000004
		 0.5 0.3819195 0.56808048 0.45000005 0.54999995 0.45000005 0.54999995 0.43191952 0.54308051
		 0.42500004 0.52499998 0.42500004 0.52499998 0.40691951 0.5930804 0.47500002 0.57499993
		 0.47500005 0.57499993 0.45691949 0.61808044 0.50000006 0.5999999 0.50000006 0.5999999
		 0.4819195 0.375 0.99308038 0.375 0.97499985 0.39308056 0.9749999 0.42500001 0.94308043
		 0.42500001 0.92499989 0.44308057 0.92499983 0.40000001 0.96808046 0.40000001 0.94999987
		 0.41808057 0.94999993 0.47500002 0.89308047 0.47500002 0.87499994 0.49308056 0.87499994
		 0.45000005 0.91808045 0.45000002 0.89999992 0.46808058 0.89999986 0.5 0.8680805 0.5
		 0.84999996 0.51808053 0.84999996 0.54999995 0.81808054 0.54999995 0.80000001 0.56808048
		 0.80000001 0.52499998 0.84308052 0.52499998 0.82499999 0.54308057 0.82500005 0.57499993
		 0.79308057 0.57499993 0.77500004 0.5930804 0.77500004 0.5999999 0.76808059 0.5999999
		 0.75000006 0.61808044 0.75000012 0.36808056 0.25 0.35000005 0.25 0.35000005 0 0.36808056
		 0 0.40000001 0.32500002 0.42500001 0.35000002 0.375 0.30000001 0.52499998 0.45000005
		 0.47500002 0.40000004 0.54999995 0.47500005 0.45000002 0.37500003 0.5 0.42500004
		 0.57499993 0.50000006 0.32500005 0 0.32500005 0.25 0.57499993 0.75000006 0.40000001
		 0.92499989 0.375 0.94999987 0.42500001 0.89999992 0.54999995 0.77500004 0.52499998
		 0.80000001 0.47500002 0.84999996 0.45000002 0.87499994 0.5 0.82499999 0.40000001
		 0.35000002 0.375 0.32500002 0.52499998 0.47500005 0.5 0.45000005 0.42500001 0.37500003
		 0.47500002 0.42500004 0.45000002 0.40000004 0.54999995 0.50000006 0.40000001 0.89999992
		 0.52499998 0.77500004 0.375 0.92499989 0.54999995 0.75000006 0.42500001 0.87499994
		 0.5 0.80000001 0.45000002 0.84999996 0.47500002 0.82499999 0.30000004 0 0.30000004
		 0.25 0.375 0.35000002 0.40000001 0.37500003 0.5 0.47500005 0.47500002 0.45000005
		 0.42500001 0.40000004 0.52499998 0.50000006 0.45000002 0.42500004 0.375 0.89999992
		 0.52499998 0.75000006 0.40000001 0.87499994 0.5 0.77500004 0.42500001 0.84999996
		 0.47500002 0.80000001 0.45000002 0.82499999 0.27500004 0 0.27500004 0.25 0.375 0.37500003
		 0.40000001 0.40000004 0.47500002 0.47500005 0.45000002 0.45000005 0.42500001 0.42500004
		 0.5 0.50000006 0.375 0.87499994 0.5 0.75000006 0.47500002 0.77500004 0.40000001 0.84999996
		 0.42500001 0.82499999 0.45000002 0.80000001 0.25000003 0 0.25000003 0.25 0.375 0.40000004
		 0.40000001 0.42500004 0.42500001 0.45000005 0.45000002 0.47500005 0.47500002 0.50000006
		 0.375 0.84999996 0.47500002 0.75000006 0.45000002 0.77500004 0.40000001 0.82499999
		 0.42500001 0.80000001 0.22500002 0 0.22500002 0.25 0.40000001 0.45000005 0.375 0.42500004
		 0.42500001 0.47500005 0.45000002 0.50000006 0.40000001 0.80000001 0.42500001 0.77500004
		 0.375 0.82499999 0.45000002 0.75000006 0.20000002 0 0.20000002 0.25 0.40000001 0.47500005
		 0.375 0.45000005 0.42500001 0.50000006 0.40000001 0.77500004 0.375 0.80000001 0.42500001
		 0.75000006 0.17500001 0 0.17500001 0.25 0.375 0.47500005 0.40000001 0.50000006 0.375
		 0.77500004 0.40000001 0.75000006 0.15000001 0 0.15000001 0.25 0.375 0.50000006 0.375
		 0.75000006 0.125 0 0.125 0.25 0.52427006 0.63749999 0.53936476 0.63749999 0.54514152
		 0.63750005 0.56023622 0.63750005 0.56601292 0.63749999 0.58110762 0.63750005 0.58688432
		 0.63749993 0.60197902 0.63749999 0.60197902 0.61250007 0.58688438 0.61250001 0.58110762
		 0.61250001 0.56601286 0.61250001 0.56023616 0.61250001 0.54514152 0.61249995 0.53936476
		 0.61250001 0.52427006 0.61249995 0.51849329 0.61249995 0.50339866 0.61250001 0.49762192
		 0.61250001 0.4825272 0.61249995 0.47675046 0.61250001 0.4616558 0.61249995 0.455879
		 0.61249995 0.44078434 0.61250001 0.43500763 0.61249995 0.41991293 0.61249995 0.4141362
		 0.61250001 0.3990415 0.61249995 0.39904153 0.63750005 0.41413623 0.63750005 0.41991293
		 0.63749999 0.43500763 0.63750005 0.44078436 0.63749999 0.45587903 0.63749999 0.4616558
		 0.63749999 0.47675049 0.63749999 0.4825272 0.63749999 0.49762186 0.63749999 0.50339866
		 0.63749999 0.51849335 0.63749999;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 150 ".vt[0:149]"  -2.27677822 0.050000001 2.000000238419 -3 0.050000001 2.000000238419
		 -3 0.050000001 2.72322202 -4.27677774 0.050000001 4 -5 0.050000001 4 -5 0.050000001 4.72322226
		 -3.27677774 0.050000001 3 -4 0.050000001 3 -4 0.050000001 3.72322226 -0.27677813 0.050000001 1.4901161e-07
		 -1.000000119209 0.050000001 1.4901161e-07 -1.000000119209 0.050000001 0.7232222 -1.2767781 0.050000001 1.000000119209
		 -2.000000238419 0.050000001 1.000000119209 -2.000000238419 0.050000001 1.72322237
		 0.72322184 0.050000001 -0.99999988 -1.4901161e-07 0.050000001 -0.99999988 -1.4901161e-07 0.050000001 -0.27677783
		 2.72322178 0.050000001 -2.99999976 1.99999988 0.050000001 -2.99999976 1.99999988 0.050000001 -2.27677798
		 1.72322178 0.050000001 -1.99999988 0.99999988 0.050000001 -1.99999988 0.99999988 0.050000001 -1.27677786
		 3.72322154 0.050000001 -3.99999976 2.99999976 0.050000001 -3.99999976 2.99999976 0.050000001 -3.27677798
		 4.72322178 0.050000001 -5 3.99999976 0.050000001 -5 3.99999976 0.050000001 -4.27677774
		 -5 -0.050000001 4.72322226 -5 -0.050000001 3.99999976 -4.27677774 -0.050000001 3.99999976
		 -3 -0.050000001 2.72322202 -3 -0.050000001 1.99999988 -2.27677774 -0.050000001 1.99999988
		 -4 -0.050000001 3.72322226 -4 -0.050000001 2.99999976 -3.27677751 -0.050000001 2.99999976
		 -1.000000119209 -0.050000001 0.7232222 -1.000000119209 -0.050000001 -1.4901161e-07
		 -0.27677783 -0.050000001 -1.4901161e-07 -2.000000238419 -0.050000001 1.72322237 -2.000000238419 -0.050000001 0.99999988
		 -1.27677786 -0.050000001 0.99999988 -1.4901161e-07 -0.050000001 -0.27677783 -1.4901161e-07 -0.050000001 -1.000000119209
		 0.72322208 -0.050000001 -1.000000119209 1.99999988 -0.050000001 -2.27677798 1.99999988 -0.050000001 -3
		 2.72322202 -0.050000001 -3 0.99999988 -0.050000001 -1.27677786 0.99999988 -0.050000001 -2.000000238419
		 1.72322214 -0.050000001 -2.000000238419 2.99999976 -0.050000001 -3.27677798 2.99999976 -0.050000001 -4
		 3.72322178 -0.050000001 -4 3.99999976 -0.050000001 -4.27677774 3.99999976 -0.050000001 -5
		 4.72322178 -0.050000001 -5 -4 0.050000001 2.000000238419 -3 0.050000001 1.000000119209
		 -5 0.050000001 3 0.99999988 0.050000001 -2.99999976 -1.000000119209 0.050000001 -0.99999988
		 1.99999988 0.050000001 -3.99999976 -2.000000238419 0.050000001 1.4901161e-07 -1.4901161e-07 0.050000001 -1.99999988
		 2.99999976 0.050000001 -5 -5 -0.050000001 2.99999976 2.99999976 -0.050000001 -5 -4 -0.050000001 1.99999988
		 -3 -0.050000001 0.99999988 1.99999988 -0.050000001 -4 0.99999988 -0.050000001 -3
		 -1.000000119209 -0.050000001 -1.000000119209 -2.000000238419 -0.050000001 -1.4901161e-07
		 -1.4901161e-07 -0.050000001 -2.000000238419 -4 0.050000001 1.000000119209 -5 0.050000001 2.000000238419
		 0.99999988 0.050000001 -3.99999976 -1.4901161e-07 0.050000001 -2.99999976 -3 0.050000001 1.4901161e-07
		 -1.000000119209 0.050000001 -1.99999988 -2.000000238419 0.050000001 -0.99999988 1.99999988 0.050000001 -5
		 -4 -0.050000001 0.99999988 0.99999988 -0.050000001 -4 -5 -0.050000001 1.99999988
		 1.99999988 -0.050000001 -5 -3 -0.050000001 -1.4901161e-07 -1.4901161e-07 -0.050000001 -3
		 -2.000000238419 -0.050000001 -1.000000119209 -1.000000119209 -0.050000001 -2.000000238419
		 -5 0.050000001 1.000000119209 -4 0.050000001 1.4901161e-07 -1.4901161e-07 0.050000001 -3.99999976
		 -1.000000119209 0.050000001 -2.99999976 -3 0.050000001 -0.99999988 0.99999988 0.050000001 -5
		 -2.000000238419 0.050000001 -1.99999988 -5 -0.050000001 0.99999988 0.99999988 -0.050000001 -5
		 -4 -0.050000001 -1.4901161e-07 -1.4901161e-07 -0.050000001 -4 -3 -0.050000001 -1.000000119209
		 -1.000000119209 -0.050000001 -3 -2.000000238419 -0.050000001 -2.000000238419 -5 0.050000001 1.4901161e-07
		 -4 0.050000001 -0.99999988 -1.000000119209 0.050000001 -3.99999976 -2.000000238419 0.050000001 -2.99999976
		 -3 0.050000001 -1.99999988 -1.4901161e-07 0.050000001 -5 -5 -0.050000001 -1.4901161e-07
		 -1.4901161e-07 -0.050000001 -5 -1.000000119209 -0.050000001 -4 -4 -0.050000001 -1.000000119209
		 -3 -0.050000001 -2.000000238419 -2.000000238419 -0.050000001 -3 -5 0.050000001 -0.99999988
		 -4 0.050000001 -1.99999988 -3 0.050000001 -2.99999976 -2.000000238419 0.050000001 -3.99999976
		 -1.000000119209 0.050000001 -5 -5 -0.050000001 -1.000000119209 -1.000000119209 -0.050000001 -5
		 -2.000000238419 -0.050000001 -4 -4 -0.050000001 -2.000000238419 -3 -0.050000001 -3
		 -4 0.050000001 -2.99999976 -5 0.050000001 -1.99999988 -3 0.050000001 -3.99999976
		 -2.000000238419 0.050000001 -5 -4 -0.050000001 -3 -3 -0.050000001 -4 -5 -0.050000001 -2.000000238419
		 -2.000000238419 -0.050000001 -5 -4 0.050000001 -3.99999976 -5 0.050000001 -2.99999976
		 -3 0.050000001 -5 -4 -0.050000001 -4 -5 -0.050000001 -3 -3 -0.050000001 -5 -5 0.050000001 -3.99999976
		 -4 0.050000001 -5 -5 -0.050000001 -4 -4 -0.050000001 -5 -5 0.050000001 -5 -5 -0.050000001 -5;
	setAttr -s 279 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 0 0 3 4 1 4 5 0 5 3 0 6 7 1 7 8 1 8 6 0
		 9 10 1 10 11 1 11 9 0 12 13 1 13 14 1 14 12 0 15 16 1 16 17 1 17 15 0 18 19 1 19 20 1
		 20 18 0 21 22 1 22 23 1 23 21 0 24 25 1 25 26 1 26 24 0 27 28 0 28 29 1 29 27 0 30 31 0
		 31 32 1 32 30 0 33 34 1 34 35 1 35 33 0 36 37 1 37 38 1 38 36 0 39 40 1 40 41 1 41 39 0
		 42 43 1 43 44 1 44 42 0 45 46 1 46 47 1 47 45 0 48 49 1 49 50 1 50 48 0 51 52 1 52 53 1
		 53 51 0 54 55 1 55 56 1 56 54 0 57 58 1 58 59 0 59 57 0 4 31 1 30 5 0 58 28 1 27 59 0
		 1 60 1 60 7 1 6 2 0 13 61 1 61 1 1 0 14 0 7 62 1 62 4 0 3 8 0 19 63 1 63 22 1 21 20 0
		 16 64 1 64 10 1 9 17 0 25 65 1 65 19 1 18 26 0 10 66 1 66 13 1 12 11 0 22 67 1 67 16 1
		 15 23 0 28 68 0 68 25 1 24 29 0 69 31 0 62 69 1 58 70 0 70 68 1 37 71 1 71 34 1 33 38 0
		 69 37 1 36 32 0 34 72 1 72 43 1 42 35 0 49 73 1 73 55 1 54 50 0 52 74 1 74 49 1 48 53 0
		 40 75 1 75 46 1 45 41 0 43 76 1 76 40 1 39 44 0 55 70 1 57 56 0 46 77 1 77 52 1 51 47 0
		 61 78 1 78 60 1 60 79 1 79 62 0 65 80 1 80 63 1 63 81 1 81 67 1 66 82 1 82 61 1 67 83 1
		 83 64 1 64 84 1 84 66 1 68 85 0 85 65 1 86 72 1 71 86 1 87 73 1 74 87 1 88 71 1 69 88 0
		 89 70 0 73 89 1 90 76 1 72 90 1 91 74 1 77 91 1 92 75 1 76 92 1 93 77 1 75 93 1 79 88 1
		 89 85 1 78 94 1 94 79 0 82 95 1 95 78 1 80 96 1 96 81 1 81 97 1 97 83 1 84 98 1 98 82 1
		 85 99 0 99 80 1;
	setAttr ".ed[166:278]" 83 100 1 100 84 1 101 86 1 88 101 0 102 89 0 87 102 1
		 103 90 1 86 103 1 104 87 1 91 104 1 105 92 1 90 105 1 106 91 1 93 106 1 107 93 1
		 92 107 1 94 101 1 102 99 1 95 108 1 108 94 0 98 109 1 109 95 1 96 110 1 110 97 1
		 97 111 1 111 100 1 100 112 1 112 98 1 99 113 0 113 96 1 114 103 1 101 114 0 115 102 0
		 104 115 1 116 104 1 106 116 1 117 105 1 103 117 1 118 107 1 105 118 1 119 106 1 107 119 1
		 108 114 1 115 113 1 109 120 1 120 108 0 112 121 1 121 109 1 111 122 1 122 112 1 110 123 1
		 123 111 1 113 124 0 124 110 1 125 117 1 114 125 0 126 115 0 116 126 1 127 116 1 119 127 1
		 128 118 1 117 128 1 129 119 1 118 129 1 120 125 1 126 124 1 122 130 1 130 121 1 121 131 1
		 131 120 0 123 132 1 132 122 1 124 133 0 133 123 1 134 129 1 128 134 1 135 127 1 129 135 1
		 136 128 1 125 136 0 137 126 0 127 137 1 131 136 1 137 133 1 132 138 1 138 130 1 130 139 1
		 139 131 0 133 140 0 140 132 1 141 135 1 134 141 1 142 134 1 136 142 0 143 137 0 135 143 1
		 139 142 1 143 140 1 138 144 1 144 139 0 140 145 0 145 138 1 146 141 1 142 146 0 147 143 0
		 141 147 1 144 146 1 147 145 1 145 148 0 148 144 0 149 147 0 146 149 0 148 149 0;
	setAttr -s 131 -ch 558 ".fc[0:130]" -type "polyFaces" 
		f 3 0 1 2
		mu 0 3 0 1 2
		f 3 3 4 5
		mu 0 3 3 4 5
		f 3 6 7 8
		mu 0 3 6 7 8
		f 3 9 10 11
		mu 0 3 9 10 11
		f 3 12 13 14
		mu 0 3 12 13 14
		f 3 15 16 17
		mu 0 3 15 16 17
		f 3 18 19 20
		mu 0 3 18 19 20
		f 3 21 22 23
		mu 0 3 21 22 23
		f 3 24 25 26
		mu 0 3 24 25 26
		f 3 27 28 29
		mu 0 3 27 28 29
		f 3 30 31 32
		mu 0 3 30 31 32
		f 3 33 34 35
		mu 0 3 33 34 35
		f 3 36 37 38
		mu 0 3 36 37 38
		f 3 39 40 41
		mu 0 3 39 40 41
		f 3 42 43 44
		mu 0 3 42 43 44
		f 3 45 46 47
		mu 0 3 45 46 47
		f 3 48 49 50
		mu 0 3 48 49 50
		f 3 51 52 53
		mu 0 3 51 52 53
		f 3 54 55 56
		mu 0 3 54 55 56
		f 3 57 58 59
		mu 0 3 57 58 59
		f 4 -5 60 -31 61
		mu 0 4 60 61 62 63
		f 4 -59 62 -28 63
		mu 0 4 59 58 28 27
		f 5 -2 64 65 -7 66
		mu 0 5 2 1 64 7 6
		f 5 -14 67 68 -1 69
		mu 0 5 14 13 65 1 0
		f 5 -8 70 71 -4 72
		mu 0 5 8 7 66 4 3
		f 5 -20 73 74 -22 75
		mu 0 5 20 19 67 22 21
		f 5 -17 76 77 -10 78
		mu 0 5 17 16 68 10 9
		f 5 -26 79 80 -19 81
		mu 0 5 26 25 69 19 18
		f 5 -11 82 83 -13 84
		mu 0 5 11 10 70 13 12
		f 5 -23 85 86 -16 87
		mu 0 5 23 22 71 16 15
		f 5 -29 88 89 -25 90
		mu 0 5 29 28 72 25 24
		f 4 91 -61 -72 92
		mu 0 4 73 62 61 74
		f 4 -89 -63 93 94
		mu 0 4 72 28 58 75
		f 5 -38 95 96 -34 97
		mu 0 5 38 37 76 34 33
		f 5 -32 -92 98 -37 99
		mu 0 5 32 31 77 37 36
		f 5 -35 100 101 -43 102
		mu 0 5 35 34 78 43 42
		f 5 -50 103 104 -55 105
		mu 0 5 50 49 79 55 54
		f 5 -53 106 107 -49 108
		mu 0 5 53 52 80 49 48
		f 5 -41 109 110 -46 111
		mu 0 5 41 40 81 46 45
		f 5 -44 112 113 -40 114
		mu 0 5 44 43 82 40 39
		f 5 -56 115 -94 -58 116
		mu 0 5 56 55 75 58 57
		f 5 -47 117 118 -52 119
		mu 0 5 47 46 83 52 51
		f 4 -65 -69 120 121
		mu 0 4 64 1 65 84
		f 4 -71 -66 122 123
		mu 0 4 66 7 64 85
		f 4 -74 -81 124 125
		mu 0 4 67 19 69 86
		f 4 -86 -75 126 127
		mu 0 4 71 22 67 87
		f 4 -68 -84 128 129
		mu 0 4 65 13 70 88
		f 4 -77 -87 130 131
		mu 0 4 68 16 71 89
		f 4 -83 -78 132 133
		mu 0 4 70 10 68 90
		f 4 -80 -90 134 135
		mu 0 4 69 25 72 91
		f 4 136 -101 -97 137
		mu 0 4 92 78 34 76
		f 4 138 -104 -108 139
		mu 0 4 93 79 49 80
		f 4 140 -96 -99 141
		mu 0 4 94 76 37 77
		f 4 142 -116 -105 143
		mu 0 4 95 75 55 79
		f 4 144 -113 -102 145
		mu 0 4 96 82 43 78
		f 4 146 -107 -119 147
		mu 0 4 97 80 52 83
		f 4 148 -110 -114 149
		mu 0 4 98 81 40 82
		f 4 150 -118 -111 151
		mu 0 4 99 83 46 81
		f 4 -142 -93 -124 152
		mu 0 4 100 73 74 101
		f 4 -135 -95 -143 153
		mu 0 4 91 72 75 95
		f 4 -123 -122 154 155
		mu 0 4 85 64 84 102
		f 4 -121 -130 156 157
		mu 0 4 84 65 88 103
		f 4 -127 -126 158 159
		mu 0 4 87 67 86 104
		f 4 -131 -128 160 161
		mu 0 4 89 71 87 105
		f 4 -129 -134 162 163
		mu 0 4 88 70 90 106
		f 4 -125 -136 164 165
		mu 0 4 86 69 91 107
		f 4 -133 -132 166 167
		mu 0 4 90 68 89 108
		f 4 168 -138 -141 169
		mu 0 4 109 92 76 94
		f 4 170 -144 -139 171
		mu 0 4 110 95 79 93
		f 4 172 -146 -137 173
		mu 0 4 111 96 78 92
		f 4 174 -140 -147 175
		mu 0 4 112 93 80 97
		f 4 176 -150 -145 177
		mu 0 4 113 98 82 96
		f 4 178 -148 -151 179
		mu 0 4 114 97 83 99
		f 4 180 -152 -149 181
		mu 0 4 115 99 81 98
		f 4 -170 -153 -156 182
		mu 0 4 116 100 101 117
		f 4 -165 -154 -171 183
		mu 0 4 107 91 95 110
		f 4 -155 -158 184 185
		mu 0 4 102 84 103 118
		f 4 -157 -164 186 187
		mu 0 4 103 88 106 119
		f 4 -161 -160 188 189
		mu 0 4 105 87 104 120
		f 4 -167 -162 190 191
		mu 0 4 108 89 105 121
		f 4 -163 -168 192 193
		mu 0 4 106 90 108 122
		f 4 -159 -166 194 195
		mu 0 4 104 86 107 123
		f 4 196 -174 -169 197
		mu 0 4 124 111 92 109
		f 4 198 -172 -175 199
		mu 0 4 125 110 93 112
		f 4 200 -176 -179 201
		mu 0 4 126 112 97 114
		f 4 202 -178 -173 203
		mu 0 4 127 113 96 111
		f 4 204 -182 -177 205
		mu 0 4 128 115 98 113
		f 4 206 -180 -181 207
		mu 0 4 129 114 99 115
		f 4 -198 -183 -186 208
		mu 0 4 130 116 117 131
		f 4 -195 -184 -199 209
		mu 0 4 123 107 110 125
		f 4 -185 -188 210 211
		mu 0 4 118 103 119 132
		f 4 -187 -194 212 213
		mu 0 4 119 106 122 133
		f 4 -193 -192 214 215
		mu 0 4 122 108 121 134
		f 4 -191 -190 216 217
		mu 0 4 121 105 120 135
		f 4 -189 -196 218 219
		mu 0 4 120 104 123 136
		f 4 220 -204 -197 221
		mu 0 4 137 127 111 124
		f 4 222 -200 -201 223
		mu 0 4 138 125 112 126
		f 4 224 -202 -207 225
		mu 0 4 139 126 114 129
		f 4 226 -206 -203 227
		mu 0 4 140 128 113 127
		f 4 228 -208 -205 229
		mu 0 4 141 129 115 128
		f 4 -222 -209 -212 230
		mu 0 4 142 130 131 143
		f 4 -219 -210 -223 231
		mu 0 4 136 123 125 138
		f 4 -213 -216 232 233
		mu 0 4 133 122 134 144
		f 4 -211 -214 234 235
		mu 0 4 132 119 133 145
		f 4 -215 -218 236 237
		mu 0 4 134 121 135 146
		f 4 -217 -220 238 239
		mu 0 4 135 120 136 147
		f 4 240 -230 -227 241
		mu 0 4 148 141 128 140
		f 4 242 -226 -229 243
		mu 0 4 149 139 129 141
		f 4 244 -228 -221 245
		mu 0 4 150 140 127 137
		f 4 246 -224 -225 247
		mu 0 4 151 138 126 139
		f 4 -246 -231 -236 248
		mu 0 4 152 142 143 153
		f 4 -239 -232 -247 249
		mu 0 4 147 136 138 151
		f 4 -233 -238 250 251
		mu 0 4 144 134 146 154
		f 4 -235 -234 252 253
		mu 0 4 145 133 144 155
		f 4 -237 -240 254 255
		mu 0 4 146 135 147 156
		f 4 256 -244 -241 257
		mu 0 4 157 149 141 148
		f 4 258 -242 -245 259
		mu 0 4 158 148 140 150
		f 4 260 -248 -243 261
		mu 0 4 159 151 139 149
		f 4 -260 -249 -254 262
		mu 0 4 160 152 153 161
		f 4 -255 -250 -261 263
		mu 0 4 156 147 151 159
		f 4 -253 -252 264 265
		mu 0 4 155 144 154 162
		f 4 -251 -256 266 267
		mu 0 4 154 146 156 163
		f 4 268 -258 -259 269
		mu 0 4 164 157 148 158
		f 4 270 -262 -257 271
		mu 0 4 165 159 149 157
		f 4 -270 -263 -266 272
		mu 0 4 166 160 161 167
		f 4 -267 -264 -271 273
		mu 0 4 163 156 159 165
		f 4 -265 -268 274 275
		mu 0 4 162 154 163 168
		f 4 276 -272 -269 277
		mu 0 4 169 165 157 164
		f 4 -278 -273 -276 278
		mu 0 4 170 166 167 171
		f 4 -275 -274 -277 -279
		mu 0 4 168 163 165 169
		f 40 -54 -109 -51 -106 -57 -117 -60 -64 -30 -91 -27 -82 -21 -76 -24 -88 -18 -79 -12
		 -85 -15 -70 -3 -67 -9 -73 -6 -62 -33 -100 -39 -98 -36 -103 -45 -115 -42 -112 -48
		 -120
		mu 0 40 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191
		 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "7F67FFD9-584D-7B36-F574-0495BE987DED";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F79159D7-0246-17B3-C3D2-21A6DDDF7C80";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "45F6C404-2745-03DE-1270-B1B5D5D90A74";
createNode displayLayerManager -n "layerManager";
	rename -uid "D58DA840-4240-6894-AA85-E78B12A55A09";
createNode displayLayer -n "defaultLayer";
	rename -uid "80A5B218-7645-B0BE-7EE1-2BAA46C46BEB";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "D08E8A40-4946-6CF0-6D34-E4B82BECF352";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "8310E47C-0747-C5A3-28B4-E787D423960D";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "7E0ACB25-B940-4869-956E-989BB99FD0B5";
	setAttr ".version" -type "string" "5.2.1.1";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "E50254F4-874B-7E59-5DA1-169380E124CD";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "303957A3-A348-D5C9-E42A-AAA585088EF1";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "C791AEE2-BE4B-91A9-3101-D59DD24CE36E";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode groupId -n "groupId5";
	rename -uid "0F1C7213-7F43-E506-432A-F789EFD3577D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "7B9D2A02-E74F-FA0C-2953-059193DED8EB";
	setAttr ".ihi" 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "9F07438C-1D48-88E1-D7ED-3ABF9547FC4D";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1656\n            -height 1020\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 820\n            -height 464\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n"
		+ "            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 822\n            -height 464\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1276\n            -height 1020\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n"
		+ "            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n"
		+ "            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n"
		+ "            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n"
		+ "                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n"
		+ "                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n"
		+ "                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1276\\n    -height 1020\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1276\\n    -height 1020\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "FD28294C-9D4B-2938-3309-85846E65A1F7";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 1 -ast 0 -aet 272.4 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId6.id" "polySurfaceShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape1.iog.og[0].gco";
connectAttr "groupId5.id" "polySurfaceShape1.ciog.cog[0].cgid";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "polySurfaceShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId6.msg" ":initialShadingGroup.gn" -na;
// End of block5.ma
