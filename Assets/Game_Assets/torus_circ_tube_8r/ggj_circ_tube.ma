//Maya ASCII 2023 scene
//Name: ggj_circ_tube.ma
//Last modified: Sat, Feb 04, 2023 12:54:58 AM
//Codeset: UTF-8
requires maya "2023";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "5.2.1.1";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2023";
fileInfo "version" "2023";
fileInfo "cutIdentifier" "202211021031-847a9f9623";
fileInfo "osv" "Mac OS X 10.16";
fileInfo "UUID" "E7167DCD-BB42-548A-897C-A99290B86B3A";
createNode transform -s -n "persp";
	rename -uid "9F3DBFF6-AE4E-002C-FEAC-4A99CCE52FAD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -7.9274513202363615 25.869611438722494 45.938897978301299 ;
	setAttr ".r" -type "double3" -33.93835272958902 -15.800000000004966 -1.6527204348973718e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "569D5882-E548-9C10-F8E8-5CA89CB39B73";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 59.892497979778824;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "2460966B-DB49-1D6F-B615-58941C967FD6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -15.363908085575726 1000.1 14.911000665834806 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "38A84933-914C-B6C5-7845-9BB7D493A665";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 86.002959702146583;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "1B1499CA-474B-F600-C26D-B6B4E7471D9B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.738406588430944 -19.454797444476895 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "7EF71168-8A4C-73B7-FDFC-A3B014E2199F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 95.845176615988066;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "73E81EA3-A044-F62F-3F8D-76A1020ABB1D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "A40AF102-7543-0F76-C850-AFAF75BB740B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pTorus1";
	rename -uid "ECED6D6C-734C-CCFF-524D-CDA10A855FDB";
	setAttr ".t" -type "double3" 25 0 0 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".rp" -type "double3" -16.499976027756929 -1.430511474609375e-06 16.499984622001648 ;
	setAttr ".rpt" -type "double3" 0 -16.499983191490173 -16.499986052513119 ;
	setAttr ".sp" -type "double3" -16.499976027756929 -1.430511474609375e-06 16.499984622001648 ;
createNode mesh -n "pTorusShape1" -p "pTorus1";
	rename -uid "442C3A36-EC46-F72F-8150-66B2486FBE9F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.61249969899654388 0.49999992176890373 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "1D901909-2047-15B4-DF6A-38A3808E3BC9";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "5F2FF9CD-CB4F-EB36-A7CD-B6B030F2E635";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "757F84A7-624D-19C5-C89D-F6BC10F19045";
createNode displayLayerManager -n "layerManager";
	rename -uid "6AA3886A-9B46-F78E-0E35-A3B80AA52846";
createNode displayLayer -n "defaultLayer";
	rename -uid "7C8B8D44-4142-5028-41FC-7AB9CE3C72E1";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "A4F94278-9243-FEA1-8D05-46A359D0014B";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "405931A1-B644-D3DB-86CB-00AC24D2F87A";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "2A7476E8-A742-12AC-7D69-21A6247068A0";
	setAttr ".version" -type "string" "5.2.1.1";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "2DDC923A-1A40-0E0C-DE05-DF8B5359EC2B";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "8426E209-D440-F1D1-E978-5C8DB9AF3222";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "279F62B2-5C4D-2951-6B4C-AEA4CCB0F960";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode polyTorus -n "polyTorus1";
	rename -uid "CBE25BBB-FE44-8912-D6F5-4FA4C8E3827D";
	setAttr ".r" 25;
	setAttr ".sr" 8;
	setAttr ".sa" 80;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "746A5798-9545-90FF-9277-1BA2B7CE53C7";
	setAttr ".dc" -type "componentList" 21 "f[0:38]" "f[79:118]" "f[159:198]" "f[239:278]" "f[319:358]" "f[399:438]" "f[479:518]" "f[559:598]" "f[639:678]" "f[719:758]" "f[799:838]" "f[879:918]" "f[959:998]" "f[1039:1078]" "f[1119:1158]" "f[1199:1238]" "f[1279:1318]" "f[1359:1398]" "f[1439:1478]" "f[1519:1558]" "f[1599]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "5019A877-DE4A-3049-857E-9882043B16EB";
	setAttr ".dc" -type "componentList" 20 "f[20:39]" "f[60:79]" "f[100:119]" "f[140:159]" "f[180:199]" "f[220:239]" "f[260:279]" "f[300:319]" "f[340:359]" "f[380:399]" "f[420:439]" "f[460:479]" "f[500:519]" "f[540:559]" "f[580:599]" "f[620:639]" "f[660:679]" "f[700:719]" "f[740:759]" "f[780:799]";
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "30CA525A-9949-8208-6CEE-84890F3849B3";
	setAttr ".ics" -type "componentList" 1 "f[0:399]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 25 0 -1.1101888786094704e-16 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 8.5000238 -16.499985 -1.4305115e-06 ;
	setAttr ".rs" 458050680;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -7.9999504089355469 -32.999977111816406 -8.0000038146972656 ;
	setAttr ".cbx" -type "double3" 24.999998353421688 7.8678131103515625e-06 8.0000009536743217 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "DCF04C41-7246-002A-009E-8AA0B53552D6";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 822\n            -height 466\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 820\n            -height 464\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n"
		+ "            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 822\n            -height 464\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1656\n            -height 1020\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n"
		+ "            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n"
		+ "            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n"
		+ "            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n"
		+ "                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n"
		+ "                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n"
		+ "                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1656\\n    -height 1020\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1656\\n    -height 1020\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "F62A5C20-FC4C-2449-12E1-E48C1C94E88F";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 1 -ast 0 -aet 272.4 ";
	setAttr ".st" 6;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "8D110A1D-E543-AF1C-2BA5-DAA29BEAC0BE";
	setAttr ".ics" -type "componentList" 1 "f[0:839]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 25 0 -1.1101888786094704e-16 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 7.9156237 -16.976561 -1.4305115e-06 ;
	setAttr ".rs" 439718978;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -9.1687507629394531 -33.953128814697266 -9.6687698364257777 ;
	setAttr ".cbx" -type "double3" 24.999998092651367 7.8678131103515625e-06 9.6687669754028356 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "A31574EF-6D4B-1FDC-EB23-8C87D08E958D";
	setAttr ".uopa" yes;
	setAttr -s 442 ".tk";
	setAttr ".tk[2]" -type "float3" -0.50108904 -4.4505655e-17 0.20043565 ;
	setAttr ".tk[3]" -type "float3" 0.36746535 5.9340872e-17 -0.26724753 ;
	setAttr ".tk[5]" -type "float3" -1.5192013 -2.9403694e-07 0 ;
	setAttr ".tk[6]" -type "float3" 1.3526086 5.7790093e-17 -0.26026344 ;
	setAttr ".tk[8]" -type "float3" -0.75258017 -1.5240033e-07 0.20365876 ;
	setAttr ".tk[9]" -type "float3" 0.22538048 2.6452106e-16 -1.1912969 ;
	setAttr ".tk[10]" -type "float3" 1.1799943 2.983985e-07 0 ;
	setAttr ".tk[12]" -type "float3" 0.55380672 1.639597e-16 -0.73840886 ;
	setAttr ".tk[13]" -type "float3" 0.55380672 1.639597e-16 -0.73840886 ;
	setAttr ".tk[15]" -type "float3" -0.71883392 3.0102342e-08 0.64076805 ;
	setAttr ".tk[17]" -type "float3" 0.13483368 2.4699745e-16 -1.1123776 ;
	setAttr ".tk[18]" -type "float3" 0.13483368 2.4699745e-16 -1.1123776 ;
	setAttr ".tk[23]" -type "float3" -0.50108904 0 0.20043565 ;
	setAttr ".tk[24]" -type "float3" 0.36746535 0 -0.26724753 ;
	setAttr ".tk[26]" -type "float3" -1.4448463 -0.50813979 0 ;
	setAttr ".tk[27]" -type "float3" 1.3526086 0 -0.26026344 ;
	setAttr ".tk[29]" -type "float3" -0.71883309 -0.26337051 0.20365876 ;
	setAttr ".tk[30]" -type "float3" 0.22538048 0 -1.1912969 ;
	setAttr ".tk[31]" -type "float3" 1.122241 0.51567721 0 ;
	setAttr ".tk[33]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[34]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[36]" -type "float3" -0.68598914 0.052021332 0.55565202 ;
	setAttr ".tk[38]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[39]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[44]" -type "float3" -0.50108904 0 0.20043565 ;
	setAttr ".tk[45]" -type "float3" 0.36746535 0 -0.26724753 ;
	setAttr ".tk[47]" -type "float3" -1.2290596 -0.96653903 0 ;
	setAttr ".tk[48]" -type "float3" 1.3526086 0 -0.26026344 ;
	setAttr ".tk[50]" -type "float3" -0.62089503 -0.50096035 0.20365876 ;
	setAttr ".tk[52]" -type "float3" 0.95463526 0.98087609 0 ;
	setAttr ".tk[54]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[55]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[57]" -type "float3" -0.59066856 0.098950393 0.30863571 ;
	setAttr ".tk[59]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[60]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[68]" -type "float3" -0.89296412 -1.3303267 0 ;
	setAttr ".tk[69]" -type "float3" 0.30302215 0 -0.50503683 ;
	setAttr ".tk[71]" -type "float3" -0.46835285 -0.68951273 0.20365876 ;
	setAttr ".tk[73]" -type "float3" 0.69358319 1.35006 0 ;
	setAttr ".tk[75]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[76]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[78]" -type "float3" -0.44220403 0.13619359 -0.076101393 ;
	setAttr ".tk[89]" -type "float3" -0.46945879 -1.563893 0 ;
	setAttr ".tk[90]" -type "float3" 0.30302215 0 -0.50503683 ;
	setAttr ".tk[92]" -type "float3" -0.2761386 -0.81057084 0.20365876 ;
	setAttr ".tk[94]" -type "float3" 0.36463824 1.5870907 0 ;
	setAttr ".tk[96]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[97]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[99]" -type "float3" -0.25512773 0.16010517 -0.56089866 ;
	setAttr ".tk[101]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[102]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[107]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[108]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[110]" -type "float3" 2.1663504e-07 -1.6443743 0 ;
	setAttr ".tk[111]" -type "float3" -0.45453322 0 1.0858297 ;
	setAttr ".tk[113]" -type "float3" -0.063067943 -0.85228467 0.20365876 ;
	setAttr ".tk[115]" -type "float3" 0 1.668766 0 ;
	setAttr ".tk[117]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[118]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[120]" -type "float3" -0.04775222 0.16834456 -1.0983002 ;
	setAttr ".tk[122]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[123]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[128]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[129]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[131]" -type "float3" 0.46945924 -1.563893 0 ;
	setAttr ".tk[132]" -type "float3" -0.45453322 0 1.0858297 ;
	setAttr ".tk[134]" -type "float3" 0.15000302 -0.81057084 0.20365876 ;
	setAttr ".tk[136]" -type "float3" -0.36463866 1.5870908 0 ;
	setAttr ".tk[138]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[139]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[141]" -type "float3" 0.15962359 0.16010523 -1.6357024 ;
	setAttr ".tk[149]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[152]" -type "float3" 0.89296412 -1.3303267 0 ;
	setAttr ".tk[153]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[155]" -type "float3" 0.84725416 -0.68951285 -0.6296522 ;
	setAttr ".tk[157]" -type "float3" -0.69358319 1.3500601 0 ;
	setAttr ".tk[159]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[160]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[162]" -type "float3" 0.34669968 0.13619363 -2.1204991 ;
	setAttr ".tk[164]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[165]" -type "float3" 0.1685421 0 -0.6404599 ;
	setAttr ".tk[169]" -type "float3" 0.12778243 0 0.53627896 ;
	setAttr ".tk[170]" -type "float3" -1.5750808 0 0.17500904 ;
	setAttr ".tk[171]" -type "float3" 0.97537029 0 -0.073088065 ;
	setAttr ".tk[173]" -type "float3" 1.2290601 -0.96653914 0 ;
	setAttr ".tk[174]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[176]" -type "float3" 0.99979615 -0.50096041 -0.6296522 ;
	setAttr ".tk[178]" -type "float3" -0.9546358 0.98087609 0 ;
	setAttr ".tk[180]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[181]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[183]" -type "float3" 0.49516436 0.098950453 -2.5052366 ;
	setAttr ".tk[185]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[186]" -type "float3" -0.331433 0 1.3594406 ;
	setAttr ".tk[187]" -type "float3" -0.29998505 0 0.049997509 ;
	setAttr ".tk[190]" -type "float3" 0.12778243 0 0.53627896 ;
	setAttr ".tk[191]" -type "float3" -1.5750808 0 0.17500904 ;
	setAttr ".tk[192]" -type "float3" 0.97537029 0 -0.073088065 ;
	setAttr ".tk[194]" -type "float3" 1.4448472 -0.50813985 0 ;
	setAttr ".tk[195]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[196]" -type "float3" -0.18516272 0 0.59252071 ;
	setAttr ".tk[197]" -type "float3" 1.0977343 -0.26337057 -0.6296522 ;
	setAttr ".tk[198]" -type "float3" -0.96284616 0 -0.59252071 ;
	setAttr ".tk[199]" -type "float3" -1.1222419 0.51567727 0 ;
	setAttr ".tk[201]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[202]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[204]" -type "float3" 0.5904845 0.052021354 -2.752254 ;
	setAttr ".tk[206]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[207]" -type "float3" -0.331433 0 1.3594406 ;
	setAttr ".tk[208]" -type "float3" -0.29998505 0 0.049997509 ;
	setAttr ".tk[211]" -type "float3" 0.12778243 -1.1907785e-16 0.53627896 ;
	setAttr ".tk[212]" -type "float3" -1.5750808 -3.8859814e-17 0.17500904 ;
	setAttr ".tk[213]" -type "float3" 0.97537029 1.622881e-17 -0.073088065 ;
	setAttr ".tk[215]" -type "float3" 1.5192015 -2.9403694e-07 0 ;
	setAttr ".tk[216]" -type "float3" -0.78280729 -1.7942523e-16 0.80805939 ;
	setAttr ".tk[217]" -type "float3" -0.18516272 -1.3156603e-16 0.59252071 ;
	setAttr ".tk[218]" -type "float3" 1.1314809 -1.5240033e-07 -0.6296522 ;
	setAttr ".tk[219]" -type "float3" -0.96284616 1.3156603e-16 -0.59252071 ;
	setAttr ".tk[220]" -type "float3" -1.1799943 2.983985e-07 0 ;
	setAttr ".tk[222]" -type "float3" -2.2134614 -3.8272368e-16 1.7236342 ;
	setAttr ".tk[223]" -type "float3" -2.2134614 -3.8272368e-16 1.7236342 ;
	setAttr ".tk[225]" -type "float3" 0.62332934 3.0102342e-08 -2.8373685 ;
	setAttr ".tk[227]" -type "float3" 0.62012351 1.4221066e-16 -0.6404599 ;
	setAttr ".tk[228]" -type "float3" -0.331433 -3.0185644e-16 1.3594406 ;
	setAttr ".tk[229]" -type "float3" -0.29998505 -1.1101677e-17 0.049997509 ;
	setAttr ".tk[232]" -type "float3" 0.12778243 0 0.53627896 ;
	setAttr ".tk[233]" -type "float3" -1.5750808 0 0.17500904 ;
	setAttr ".tk[234]" -type "float3" 0.97537029 0 -0.073088065 ;
	setAttr ".tk[236]" -type "float3" 1.4448472 0.50813925 0 ;
	setAttr ".tk[237]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[238]" -type "float3" -0.18516272 0 0.59252071 ;
	setAttr ".tk[239]" -type "float3" 1.0977343 0.26337028 -0.6296522 ;
	setAttr ".tk[240]" -type "float3" -0.96284616 0 -0.59252071 ;
	setAttr ".tk[241]" -type "float3" -1.1222419 -0.51567668 0 ;
	setAttr ".tk[243]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[244]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[246]" -type "float3" 0.5904845 -0.052021306 -2.752254 ;
	setAttr ".tk[248]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[249]" -type "float3" -0.331433 0 1.3594406 ;
	setAttr ".tk[250]" -type "float3" -0.29998505 0 0.049997509 ;
	setAttr ".tk[253]" -type "float3" 0.12778243 0 0.53627896 ;
	setAttr ".tk[254]" -type "float3" -1.5750808 0 0.17500904 ;
	setAttr ".tk[255]" -type "float3" 0.97537029 0 -0.073088065 ;
	setAttr ".tk[257]" -type "float3" 1.2290601 0.96653867 0 ;
	setAttr ".tk[258]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[260]" -type "float3" 0.99979615 0.50096017 -0.6296522 ;
	setAttr ".tk[262]" -type "float3" -0.9546358 -0.98087585 0 ;
	setAttr ".tk[264]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[265]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[267]" -type "float3" 0.49516436 -0.098950393 -2.5052366 ;
	setAttr ".tk[269]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[270]" -type "float3" -0.331433 0 1.3594406 ;
	setAttr ".tk[271]" -type "float3" -0.29998505 0 0.049997509 ;
	setAttr ".tk[275]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[278]" -type "float3" 0.89296448 1.3303264 0 ;
	setAttr ".tk[279]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[281]" -type "float3" 0.84725434 0.68951267 -0.6296522 ;
	setAttr ".tk[283]" -type "float3" -0.69358361 -1.3500596 0 ;
	setAttr ".tk[285]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[286]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[288]" -type "float3" 0.34669983 -0.13619354 -2.1204994 ;
	setAttr ".tk[290]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[291]" -type "float3" 0.1685421 0 -0.6404599 ;
	setAttr ".tk[296]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[297]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[299]" -type "float3" 0.46945924 1.563893 0 ;
	setAttr ".tk[300]" -type "float3" -0.45453322 0 1.0858297 ;
	setAttr ".tk[302]" -type "float3" 0.15000302 0.81057084 0.20365876 ;
	setAttr ".tk[304]" -type "float3" -0.36463866 -1.5870908 0 ;
	setAttr ".tk[306]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[307]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[309]" -type "float3" 0.15962359 -0.16010517 -1.6357024 ;
	setAttr ".tk[317]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[318]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[320]" -type "float3" 2.1663504e-07 1.6443743 0 ;
	setAttr ".tk[321]" -type "float3" -0.45453322 0 1.0858297 ;
	setAttr ".tk[323]" -type "float3" -0.063067943 0.85228467 0.20365876 ;
	setAttr ".tk[325]" -type "float3" 0 -1.668766 0 ;
	setAttr ".tk[327]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[328]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[330]" -type "float3" -0.04775222 -0.16834456 -1.0983002 ;
	setAttr ".tk[332]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[333]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[341]" -type "float3" -0.46945924 1.563893 0 ;
	setAttr ".tk[342]" -type "float3" 0.30302215 0 -0.50503683 ;
	setAttr ".tk[344]" -type "float3" -0.27613884 0.81057084 0.20365876 ;
	setAttr ".tk[346]" -type "float3" 0.36463839 -1.5870908 0 ;
	setAttr ".tk[348]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[349]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[351]" -type "float3" -0.25512812 -0.16010523 -0.56089813 ;
	setAttr ".tk[353]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[354]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[362]" -type "float3" -0.8929649 1.3303267 0 ;
	setAttr ".tk[363]" -type "float3" 0.30302215 0 -0.50503683 ;
	setAttr ".tk[365]" -type "float3" -0.46835333 0.68951285 0.20365876 ;
	setAttr ".tk[367]" -type "float3" 0.69358349 -1.3500601 0 ;
	setAttr ".tk[369]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[370]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[372]" -type "float3" -0.4422043 -0.13619363 -0.076100558 ;
	setAttr ".tk[380]" -type "float3" -0.50108904 0 0.20043565 ;
	setAttr ".tk[381]" -type "float3" 0.36746535 0 -0.26724753 ;
	setAttr ".tk[383]" -type "float3" -1.2290605 0.96653891 0 ;
	setAttr ".tk[384]" -type "float3" 1.3526086 0 -0.26026344 ;
	setAttr ".tk[386]" -type "float3" -0.62089562 0.50096029 0.20365876 ;
	setAttr ".tk[388]" -type "float3" 0.95463604 -0.98087603 0 ;
	setAttr ".tk[390]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[391]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[393]" -type "float3" -0.59066898 -0.098950408 0.30863696 ;
	setAttr ".tk[395]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[396]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[401]" -type "float3" -0.50108904 0 0.20043565 ;
	setAttr ".tk[402]" -type "float3" 0.36746535 0 -0.26724753 ;
	setAttr ".tk[404]" -type "float3" -1.4448472 0.50813943 0 ;
	setAttr ".tk[405]" -type "float3" 1.3526086 0 -0.26026344 ;
	setAttr ".tk[407]" -type "float3" -0.71883368 0.26337036 0.20365876 ;
	setAttr ".tk[408]" -type "float3" 0.22538048 0 -1.1912969 ;
	setAttr ".tk[409]" -type "float3" 1.1222421 -0.51567692 0 ;
	setAttr ".tk[411]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[412]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[414]" -type "float3" -0.68598956 -0.052021306 0.55565327 ;
	setAttr ".tk[416]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[417]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[424]" -type "float3" -0.50108904 -4.4505655e-17 0.20043565 ;
	setAttr ".tk[425]" -type "float3" -0.50108904 0 0.20043565 ;
	setAttr ".tk[426]" -type "float3" 0.36746535 5.9340872e-17 -0.26724753 ;
	setAttr ".tk[427]" -type "float3" 0.36746535 0 -0.26724753 ;
	setAttr ".tk[430]" -type "float3" -1.5192013 -2.9403694e-07 0 ;
	setAttr ".tk[431]" -type "float3" -1.4448463 -0.50813979 0 ;
	setAttr ".tk[432]" -type "float3" 1.3526086 5.7790093e-17 -0.26026344 ;
	setAttr ".tk[433]" -type "float3" 1.3526086 0 -0.26026344 ;
	setAttr ".tk[436]" -type "float3" -0.75258017 -1.5240033e-07 0.20365876 ;
	setAttr ".tk[437]" -type "float3" -0.71883309 -0.26337051 0.20365876 ;
	setAttr ".tk[438]" -type "float3" 0.22538048 2.6452106e-16 -1.1912969 ;
	setAttr ".tk[439]" -type "float3" 0.22538048 0 -1.1912969 ;
	setAttr ".tk[440]" -type "float3" 1.1799943 2.983985e-07 0 ;
	setAttr ".tk[441]" -type "float3" 1.122241 0.51567721 0 ;
	setAttr ".tk[444]" -type "float3" 0.55380672 1.639597e-16 -0.73840886 ;
	setAttr ".tk[445]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[446]" -type "float3" 0.55380672 1.639597e-16 -0.73840886 ;
	setAttr ".tk[447]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[450]" -type "float3" -0.71883392 3.0102342e-08 0.64076805 ;
	setAttr ".tk[451]" -type "float3" -0.68598914 0.052021332 0.55565202 ;
	setAttr ".tk[454]" -type "float3" 0.13483368 2.4699745e-16 -1.1123776 ;
	setAttr ".tk[455]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[456]" -type "float3" 0.13483368 2.4699745e-16 -1.1123776 ;
	setAttr ".tk[457]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[464]" -type "float3" -0.50108904 0 0.20043565 ;
	setAttr ".tk[465]" -type "float3" 0.36746535 0 -0.26724753 ;
	setAttr ".tk[467]" -type "float3" -1.2290596 -0.96653903 0 ;
	setAttr ".tk[468]" -type "float3" 1.3526086 0 -0.26026344 ;
	setAttr ".tk[470]" -type "float3" -0.62089503 -0.50096035 0.20365876 ;
	setAttr ".tk[472]" -type "float3" 0.95463526 0.98087609 0 ;
	setAttr ".tk[474]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[475]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[477]" -type "float3" -0.59066856 0.098950393 0.30863571 ;
	setAttr ".tk[479]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[480]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[488]" -type "float3" -0.89296412 -1.3303267 0 ;
	setAttr ".tk[489]" -type "float3" 0.30302215 0 -0.50503683 ;
	setAttr ".tk[491]" -type "float3" -0.46835285 -0.68951273 0.20365876 ;
	setAttr ".tk[493]" -type "float3" 0.69358319 1.35006 0 ;
	setAttr ".tk[495]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[496]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[498]" -type "float3" -0.44220403 0.13619359 -0.076101393 ;
	setAttr ".tk[509]" -type "float3" -0.46945879 -1.563893 0 ;
	setAttr ".tk[510]" -type "float3" 0.30302215 0 -0.50503683 ;
	setAttr ".tk[512]" -type "float3" -0.2761386 -0.81057084 0.20365876 ;
	setAttr ".tk[514]" -type "float3" 0.36463824 1.5870907 0 ;
	setAttr ".tk[516]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[517]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[519]" -type "float3" -0.25512773 0.16010517 -0.56089866 ;
	setAttr ".tk[521]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[522]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[527]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[528]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[530]" -type "float3" 2.1663504e-07 -1.6443743 0 ;
	setAttr ".tk[531]" -type "float3" -0.45453322 0 1.0858297 ;
	setAttr ".tk[533]" -type "float3" -0.063067943 -0.85228467 0.20365876 ;
	setAttr ".tk[535]" -type "float3" 0 1.668766 0 ;
	setAttr ".tk[537]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[538]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[540]" -type "float3" -0.04775222 0.16834456 -1.0983002 ;
	setAttr ".tk[542]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[543]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[548]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[549]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[551]" -type "float3" 0.46945924 -1.563893 0 ;
	setAttr ".tk[552]" -type "float3" -0.45453322 0 1.0858297 ;
	setAttr ".tk[554]" -type "float3" 0.15000302 -0.81057084 0.20365876 ;
	setAttr ".tk[556]" -type "float3" -0.36463866 1.5870908 0 ;
	setAttr ".tk[558]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[559]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[561]" -type "float3" 0.15962359 0.16010523 -1.6357024 ;
	setAttr ".tk[569]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[572]" -type "float3" 0.89296412 -1.3303267 0 ;
	setAttr ".tk[573]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[575]" -type "float3" 0.84725416 -0.68951285 -0.6296522 ;
	setAttr ".tk[577]" -type "float3" -0.69358319 1.3500601 0 ;
	setAttr ".tk[579]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[580]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[582]" -type "float3" 0.34669968 0.13619363 -2.1204991 ;
	setAttr ".tk[584]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[585]" -type "float3" 0.1685421 0 -0.6404599 ;
	setAttr ".tk[589]" -type "float3" 0.12778243 0 0.53627896 ;
	setAttr ".tk[590]" -type "float3" -1.5750808 0 0.17500904 ;
	setAttr ".tk[591]" -type "float3" 0.97537029 0 -0.073088065 ;
	setAttr ".tk[593]" -type "float3" 1.2290601 -0.96653914 0 ;
	setAttr ".tk[594]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[596]" -type "float3" 0.99979615 -0.50096041 -0.6296522 ;
	setAttr ".tk[598]" -type "float3" -0.9546358 0.98087609 0 ;
	setAttr ".tk[600]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[601]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[603]" -type "float3" 0.49516436 0.098950453 -2.5052366 ;
	setAttr ".tk[605]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[606]" -type "float3" -0.331433 0 1.3594406 ;
	setAttr ".tk[607]" -type "float3" -0.29998505 0 0.049997509 ;
	setAttr ".tk[610]" -type "float3" 0.12778243 0 0.53627896 ;
	setAttr ".tk[611]" -type "float3" -1.5750808 0 0.17500904 ;
	setAttr ".tk[612]" -type "float3" 0.97537029 0 -0.073088065 ;
	setAttr ".tk[614]" -type "float3" 1.4448472 -0.50813985 0 ;
	setAttr ".tk[615]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[616]" -type "float3" -0.18516272 0 0.59252071 ;
	setAttr ".tk[617]" -type "float3" 1.0977343 -0.26337057 -0.6296522 ;
	setAttr ".tk[618]" -type "float3" -0.96284616 0 -0.59252071 ;
	setAttr ".tk[619]" -type "float3" -1.1222419 0.51567727 0 ;
	setAttr ".tk[621]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[622]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[624]" -type "float3" 0.5904845 0.052021354 -2.752254 ;
	setAttr ".tk[626]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[627]" -type "float3" -0.331433 0 1.3594406 ;
	setAttr ".tk[628]" -type "float3" -0.29998505 0 0.049997509 ;
	setAttr ".tk[631]" -type "float3" 0.12778243 -1.1907785e-16 0.53627896 ;
	setAttr ".tk[632]" -type "float3" -1.5750808 -3.8859814e-17 0.17500904 ;
	setAttr ".tk[633]" -type "float3" 0.97537029 1.622881e-17 -0.073088065 ;
	setAttr ".tk[635]" -type "float3" 1.5192015 -2.9403694e-07 0 ;
	setAttr ".tk[636]" -type "float3" -0.78280729 -1.7942523e-16 0.80805939 ;
	setAttr ".tk[637]" -type "float3" -0.18516272 -1.3156603e-16 0.59252071 ;
	setAttr ".tk[638]" -type "float3" 1.1314809 -1.5240033e-07 -0.6296522 ;
	setAttr ".tk[639]" -type "float3" -0.96284616 1.3156603e-16 -0.59252071 ;
	setAttr ".tk[640]" -type "float3" -1.1799943 2.983985e-07 0 ;
	setAttr ".tk[642]" -type "float3" -2.2134614 -3.8272368e-16 1.7236342 ;
	setAttr ".tk[643]" -type "float3" -2.2134614 -3.8272368e-16 1.7236342 ;
	setAttr ".tk[645]" -type "float3" 0.62332934 3.0102342e-08 -2.8373685 ;
	setAttr ".tk[647]" -type "float3" 0.62012351 1.4221066e-16 -0.6404599 ;
	setAttr ".tk[648]" -type "float3" -0.331433 -3.0185644e-16 1.3594406 ;
	setAttr ".tk[649]" -type "float3" -0.29998505 -1.1101677e-17 0.049997509 ;
	setAttr ".tk[652]" -type "float3" 0.12778243 0 0.53627896 ;
	setAttr ".tk[653]" -type "float3" -1.5750808 0 0.17500904 ;
	setAttr ".tk[654]" -type "float3" 0.97537029 0 -0.073088065 ;
	setAttr ".tk[656]" -type "float3" 1.4448472 0.50813925 0 ;
	setAttr ".tk[657]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[658]" -type "float3" -0.18516272 0 0.59252071 ;
	setAttr ".tk[659]" -type "float3" 1.0977343 0.26337028 -0.6296522 ;
	setAttr ".tk[660]" -type "float3" -0.96284616 0 -0.59252071 ;
	setAttr ".tk[661]" -type "float3" -1.1222419 -0.51567668 0 ;
	setAttr ".tk[663]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[664]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[666]" -type "float3" 0.5904845 -0.052021306 -2.752254 ;
	setAttr ".tk[668]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[669]" -type "float3" -0.331433 0 1.3594406 ;
	setAttr ".tk[670]" -type "float3" -0.29998505 0 0.049997509 ;
	setAttr ".tk[673]" -type "float3" 0.12778243 0 0.53627896 ;
	setAttr ".tk[674]" -type "float3" -1.5750808 0 0.17500904 ;
	setAttr ".tk[675]" -type "float3" 0.97537029 0 -0.073088065 ;
	setAttr ".tk[677]" -type "float3" 1.2290601 0.96653867 0 ;
	setAttr ".tk[678]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[680]" -type "float3" 0.99979615 0.50096017 -0.6296522 ;
	setAttr ".tk[682]" -type "float3" -0.9546358 -0.98087585 0 ;
	setAttr ".tk[684]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[685]" -type "float3" -2.2134614 0 1.7236342 ;
	setAttr ".tk[687]" -type "float3" 0.49516436 -0.098950393 -2.5052366 ;
	setAttr ".tk[689]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[690]" -type "float3" -0.331433 0 1.3594406 ;
	setAttr ".tk[691]" -type "float3" -0.29998505 0 0.049997509 ;
	setAttr ".tk[695]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[698]" -type "float3" 0.89296448 1.3303264 0 ;
	setAttr ".tk[699]" -type "float3" -0.78280729 0 0.80805939 ;
	setAttr ".tk[701]" -type "float3" 0.84725434 0.68951267 -0.6296522 ;
	setAttr ".tk[703]" -type "float3" -0.69358361 -1.3500596 0 ;
	setAttr ".tk[705]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[706]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[708]" -type "float3" 0.34669983 -0.13619354 -2.1204994 ;
	setAttr ".tk[710]" -type "float3" 0.62012351 0 -0.6404599 ;
	setAttr ".tk[711]" -type "float3" 0.1685421 0 -0.6404599 ;
	setAttr ".tk[716]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[717]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[719]" -type "float3" 0.46945924 1.563893 0 ;
	setAttr ".tk[720]" -type "float3" -0.45453322 0 1.0858297 ;
	setAttr ".tk[722]" -type "float3" 0.15000302 0.81057084 0.20365876 ;
	setAttr ".tk[724]" -type "float3" -0.36463866 -1.5870908 0 ;
	setAttr ".tk[726]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[727]" -type "float3" -0.66621298 0 0.26890707 ;
	setAttr ".tk[729]" -type "float3" 0.15962359 -0.16010517 -1.6357024 ;
	setAttr ".tk[737]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[738]" -type "float3" -0.43427718 0 0.46768311 ;
	setAttr ".tk[740]" -type "float3" 2.1663504e-07 1.6443743 0 ;
	setAttr ".tk[741]" -type "float3" -0.45453322 0 1.0858297 ;
	setAttr ".tk[743]" -type "float3" -0.063067943 0.85228467 0.20365876 ;
	setAttr ".tk[745]" -type "float3" 0 -1.668766 0 ;
	setAttr ".tk[747]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[748]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[750]" -type "float3" -0.04775222 -0.16834456 -1.0983002 ;
	setAttr ".tk[752]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[753]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[761]" -type "float3" -0.46945924 1.563893 0 ;
	setAttr ".tk[762]" -type "float3" 0.30302215 0 -0.50503683 ;
	setAttr ".tk[764]" -type "float3" -0.27613884 0.81057084 0.20365876 ;
	setAttr ".tk[766]" -type "float3" 0.36463839 -1.5870908 0 ;
	setAttr ".tk[768]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[769]" -type "float3" -1.4398971 0 0.84917021 ;
	setAttr ".tk[771]" -type "float3" -0.25512812 -0.16010523 -0.56089813 ;
	setAttr ".tk[773]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[774]" -type "float3" -0.80900198 0 0.10112526 ;
	setAttr ".tk[782]" -type "float3" -0.8929649 1.3303267 0 ;
	setAttr ".tk[783]" -type "float3" 0.30302215 0 -0.50503683 ;
	setAttr ".tk[785]" -type "float3" -0.46835333 0.68951285 0.20365876 ;
	setAttr ".tk[787]" -type "float3" 0.69358349 -1.3500601 0 ;
	setAttr ".tk[789]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[790]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[792]" -type "float3" -0.4422043 -0.13619363 -0.076100558 ;
	setAttr ".tk[800]" -type "float3" -0.50108904 0 0.20043565 ;
	setAttr ".tk[801]" -type "float3" 0.36746535 0 -0.26724753 ;
	setAttr ".tk[803]" -type "float3" -1.2290605 0.96653891 0 ;
	setAttr ".tk[804]" -type "float3" 1.3526086 0 -0.26026344 ;
	setAttr ".tk[806]" -type "float3" -0.62089562 0.50096029 0.20365876 ;
	setAttr ".tk[808]" -type "float3" 0.95463604 -0.98087603 0 ;
	setAttr ".tk[810]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[811]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[813]" -type "float3" -0.59066898 -0.098950408 0.30863696 ;
	setAttr ".tk[815]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[816]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[821]" -type "float3" -0.50108904 0 0.20043565 ;
	setAttr ".tk[822]" -type "float3" 0.36746535 0 -0.26724753 ;
	setAttr ".tk[824]" -type "float3" -1.4448472 0.50813943 0 ;
	setAttr ".tk[825]" -type "float3" 1.3526086 0 -0.26026344 ;
	setAttr ".tk[827]" -type "float3" -0.71883368 0.26337036 0.20365876 ;
	setAttr ".tk[828]" -type "float3" 0.22538048 0 -1.1912969 ;
	setAttr ".tk[829]" -type "float3" 1.1222421 -0.51567692 0 ;
	setAttr ".tk[831]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[832]" -type "float3" 0.55380672 0 -0.73840886 ;
	setAttr ".tk[834]" -type "float3" -0.68598956 -0.052021306 0.55565327 ;
	setAttr ".tk[836]" -type "float3" 0.13483368 0 -1.1123776 ;
	setAttr ".tk[837]" -type "float3" 0.13483368 0 -1.1123776 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "0D38CE4B-4640-C6F5-9915-C69C72F91DB6";
	setAttr ".ics" -type "componentList" 1 "f[0:1679]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 25 0 -1.1101888786094704e-16 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 7.9156237 -16.976561 -1.4305115e-06 ;
	setAttr ".rs" 316548314;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -9.1687507629394531 -33.953128814697266 -9.6687698364257777 ;
	setAttr ".cbx" -type "double3" 24.999998092651367 7.8678131103515625e-06 9.6687669754028356 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "C3DD5AB4-F443-60B7-1BEB-CF8EA6B760B9";
	setAttr ".ics" -type "componentList" 1 "f[0:1679]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 25 0 -1.1101888786094704e-16 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 7.9156237 -16.976561 -9.5367432e-07 ;
	setAttr ".rs" 1860186844;
	setAttr ".lt" -type "double3" -2.0122792321330962e-16 2.8622937353617317e-17 0.01 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -9.1687507629394531 -33.953125 -9.6687736511230433 ;
	setAttr ".cbx" -type "double3" 24.999998092651367 5.5961936606290856e-06 9.6687717437744176 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "44D8B331-5D40-843A-8906-77A61475A2B7";
	setAttr ".uopa" yes;
	setAttr -s 1680 ".tk";
	setAttr ".tk[1680:1845]" -type "float3"  0 6.2527761e-13 9.5367432e-06 -1.4901161e-08
		 6.2527761e-13 5.7220459e-06 5.9604645e-08 -8.3446503e-07 1.0490417e-05 -7.4505806e-08
		 -8.3446503e-07 -8.5830688e-06 3.7252903e-08 6.2527761e-13 -4.2915344e-06 2.0861626e-07
		 -8.3446503e-07 -5.2452087e-06 5.9604645e-08 6.2527761e-13 -2.3841858e-06 -1.7881393e-07
		 -8.3446503e-07 5.2452087e-06 4.7683716e-07 6.2527761e-13 -1.9073486e-06 -2.9802322e-08
		 -8.3446503e-07 -4.7683716e-07 -2.9802322e-08 -1.7053026e-13 -2.8610229e-06 -4.4703484e-08
		 -1.1920929e-06 2.3841858e-06 9.5367432e-07 6.2527761e-13 -1.4305115e-06 -1.3113022e-06
		 -8.3446503e-07 -2.8610229e-06 -1.3113022e-06 6.2527761e-13 -5.2452087e-06 -1.1920929e-07
		 -8.3446503e-07 -4.7683716e-07 -8.3446503e-07 -3.9790393e-13 7.1525574e-07 -9.5367432e-07
		 0 -5.0067902e-06 -1.4305115e-06 6.2527761e-13 1.4305115e-06 0 -8.3446503e-07 -1.9073486e-06
		 -2.3841858e-06 -1.080025e-12 2.1457672e-06 -2.3841858e-06 0 2.3841858e-07 3.5762787e-06
		 6.2527761e-13 4.7683716e-07 2.8610229e-06 -8.3446503e-07 7.1525574e-07 -9.5367432e-07
		 6.2527761e-13 1.9073486e-06 -1.9073486e-06 -8.3446503e-07 2.3841858e-06 2.8610229e-06
		 6.2527761e-13 3.5762787e-07 -9.5367432e-07 -8.3446503e-07 -1.1920929e-06 1.9073486e-06
		 6.2527761e-13 -1.1920929e-06 2.8610229e-06 -8.3446503e-07 5.9604645e-08 -9.5367432e-07
		 1.080025e-12 -2.0861626e-07 -3.3378601e-06 8.3446503e-07 5.9604645e-08 4.7683716e-07
		 6.2527761e-13 4.1723251e-07 -9.5367432e-07 -8.3446503e-07 -1.6391277e-07 8.5830688e-06
		 6.2527761e-13 9.5367432e-07 3.8146973e-06 -8.3446503e-07 -3.5762787e-07 1.0490417e-05
		 6.2527761e-13 4.7683716e-07 -9.5367432e-07 -8.3446503e-07 2.9802322e-07 -5.7220459e-06
		 6.2527761e-13 -3.7252903e-09 8.5830688e-06 -8.3446503e-07 5.9604645e-08 0 6.2527761e-13
		 2.7939677e-09 0 -8.3446503e-07 7.4505806e-08 -2.9802322e-07 -1.6689301e-06 1.0490417e-05
		 -4.1723251e-07 -1.6689301e-06 6.6757202e-06 1.013279e-06 -1.6689301e-06 -6.1988831e-06
		 -8.9406967e-08 -1.6689301e-06 -3.3378601e-06 5.9604645e-08 -1.6689301e-06 8.5830688e-06
		 7.1525574e-07 1.4305115e-06 -4.2915344e-06 -5.9604645e-07 -1.6689301e-06 1.4305115e-06
		 -7.1525574e-07 -1.6689301e-06 -2.8610229e-06 7.7486038e-07 0 -4.0531158e-06 -5.9604645e-07
		 -1.6689301e-06 1.4305115e-06 -1.9073486e-06 -4.0531158e-06 1.9073486e-06 1.9073486e-06
		 -1.6689301e-06 -8.3446503e-07 -1.9073486e-06 -1.6689301e-06 -1.1920929e-06 -1.9073486e-06
		 -1.6689301e-06 -9.5367432e-07 -1.4305115e-06 -1.6689301e-06 -8.9406967e-08 -3.3378601e-06
		 -2.8610229e-06 1.4901161e-08 1.4305115e-06 -1.6689301e-06 1.4901161e-07 7.6293945e-06
		 -1.6689301e-06 -7.4505806e-09 0 -1.6689301e-06 7.4505806e-08 4.7683716e-06 -1.6689301e-06
		 1.7881393e-07 0 -1.6689301e-06 -1.1920929e-07 1.1920929e-06 -1.4305115e-06 1.0490417e-05
		 5.9604645e-07 -1.4305115e-06 -7.6293945e-06 1.1920929e-07 -1.4305115e-06 3.3378601e-06
		 1.1920929e-07 -1.4305115e-06 -6.6757202e-06 -1.1920929e-07 -1.4305115e-06 6.6757202e-06
		 4.7683716e-07 -1.6689301e-06 -1.4305115e-06 -2.682209e-07 -1.4305115e-06 1.4305115e-06
		 1.4901161e-08 -1.4305115e-06 4.7683716e-07 -5.2154064e-08 -3.0994415e-06 -2.3841858e-07
		 -4.1723251e-07 -1.4305115e-06 4.7683716e-07 -8.3446503e-07 -3.8146973e-06 2.3841858e-07
		 1.1920929e-06 -1.4305115e-06 -5.364418e-07 1.1920929e-06 -1.4305115e-06 -5.9604645e-07
		 1.9073486e-06 -1.4305115e-06 7.4505806e-08 4.7683716e-06 -1.4305115e-06 1.7881393e-07
		 -3.8146973e-06 1.6689301e-06 -9.5367432e-07 3.3378601e-06 -1.4305115e-06 -7.1525574e-07
		 -1.001358e-05 -1.4305115e-06 -1.3113022e-06 -4.2915344e-06 -1.4305115e-06 2.0265579e-06
		 6.6757202e-06 -1.4305115e-06 1.3113022e-06 0 -1.4305115e-06 2.3841858e-07 -3.8146973e-06
		 9.5367432e-07 9.5367432e-06 4.7683716e-07 9.5367432e-07 -6.6757202e-06 -1.6689301e-06
		 9.5367432e-07 9.5367432e-06 1.4305115e-06 9.5367432e-07 -8.1062317e-06 1.6689301e-06
		 9.5367432e-07 -2.8610229e-06 -2.6226044e-06 -1.1920929e-06 -2.3841858e-06 9.5367432e-07
		 9.5367432e-07 -4.0531158e-06 -9.5367432e-07 9.5367432e-07 4.7683716e-07 6.5565109e-07
		 -2.6226044e-06 2.0265579e-06 1.8626451e-09 9.5367432e-07 -1.0728836e-06 -1.0728836e-06
		 2.8610229e-06 0 1.4305115e-06 9.5367432e-07 4.4703484e-08 -9.5367432e-07 9.5367432e-07
		 1.1920929e-07 1.1920929e-06 9.5367432e-07 0 -2.8610229e-06 9.5367432e-07 -1.9073486e-06
		 4.7683716e-07 2.3841858e-06 -4.7683716e-07 -1.9073486e-06 9.5367432e-07 -1.6689301e-06
		 7.6293945e-06 9.5367432e-07 -1.4305115e-06 -1.0490417e-05 9.5367432e-07 2.6226044e-06
		 -1.0490417e-05 9.5367432e-07 -1.6689301e-06 0 9.5367432e-07 2.1457672e-06 2.3841858e-06
		 4.7683716e-07 1.0490417e-05 1.9073486e-06 4.7683716e-07 2.8610229e-06 2.3841858e-06
		 4.7683716e-07 -9.5367432e-07 5.2452087e-06 4.7683716e-07 4.2915344e-06 3.0994415e-06
		 4.7683716e-07 -1.9073486e-06 -4.0531158e-06 -2.3841858e-07 -1.9073486e-06 3.0994415e-06
		 4.7683716e-07 2.3841858e-06 1.1920929e-06 4.7683716e-07 1.4305115e-06 -4.7683716e-07
		 -4.7683716e-06 0 0 4.7683716e-07 -3.2782555e-07 2.0861626e-07 4.2915344e-06 -1.7881393e-07
		 5.6624413e-07 4.7683716e-07 -3.5762787e-07 2.9802322e-07 4.7683716e-07 -7.1525574e-07
		 -1.1920929e-07 4.7683716e-07 -2.6226044e-06 1.1920929e-06 4.7683716e-07 2.3841858e-06
		 -4.2915344e-06 1.9073486e-06 1.4305115e-06 -4.7683716e-07 4.7683716e-07 3.5762787e-06
		 -3.3378601e-06 4.7683716e-07 1.9073486e-06 1.9073486e-06 4.7683716e-07 1.4305115e-06
		 2.8610229e-06 4.7683716e-07 -3.3378601e-06 0 4.7683716e-07 -4.2915344e-06 -6.6757202e-06
		 4.7683716e-07 1.9073486e-06 -2.3841858e-06 4.7683716e-07 -1.0490417e-05 -2.3841858e-06
		 4.7683716e-07 6.6757202e-06 3.8146973e-06 4.7683716e-07 -2.8610229e-06 -4.2915344e-06
		 4.7683716e-07 1.4305115e-06 -9.5367432e-07 -1.4305115e-06 -1.4305115e-06 4.7683716e-07
		 4.7683716e-07 8.3446503e-07 -3.3378601e-06 4.7683716e-07 7.1525574e-07 2.1457672e-06
		 -2.6226044e-06 2.9802322e-07 1.9073486e-06 4.7683716e-07 -3.2782555e-07 4.7683716e-07
		 2.3841858e-06 1.1920929e-06 -8.9406967e-08 4.7683716e-07 1.4305115e-06 2.9802322e-08
		 4.7683716e-07 2.6226044e-06 -1.1920929e-07 4.7683716e-07 -4.0531158e-06 -1.4305115e-06
		 4.7683716e-07 9.5367432e-07 -3.8146973e-06 2.3841858e-06 -5.2452087e-06 -2.8610229e-06
		 4.7683716e-07 6.6757202e-06 -3.8146973e-06 4.7683716e-07 6.1988831e-06 6.6757202e-06
		 4.7683716e-07 -8.1062317e-06 -7.6293945e-06 4.7683716e-07 2.3841858e-06 0 4.7683716e-07
		 -4.7683716e-06 -4.7683716e-06 -1.4305115e-06 1.9073486e-06 1.4305115e-06 -1.4305115e-06
		 7.6293945e-06 -4.7683716e-07 -1.4305115e-06 4.7683716e-07 1.9073486e-06 -1.4305115e-06
		 -4.7683716e-06 5.2452087e-06 -1.4305115e-06 5.2452087e-06 -1.4305115e-06 -1.4305115e-06
		 3.8146973e-06 0 -1.4305115e-06 1.0728836e-06 4.2915344e-06 -1.4305115e-06 -3.5762787e-07
		 2.6226044e-06 -3.0994415e-06 1.4901161e-08 4.7683716e-07 -1.4305115e-06 9.5367432e-07
		 -4.7683716e-07 -3.3378601e-06 2.3841858e-07 -7.1525574e-07 -1.4305115e-06 3.3378601e-06
		 -1.7881393e-07 -1.4305115e-06 -2.8610229e-06 4.7683716e-07 -1.4305115e-06 2.3841858e-06
		 -1.5497208e-06 -1.4305115e-06 6.6757202e-06 4.2915344e-06 1.4305115e-06 4.7683716e-07
		 2.8610229e-06 -1.4305115e-06 -1.4305115e-06 -6.6757202e-06 -1.4305115e-06 5.2452087e-06
		 -3.3378601e-06 -1.4305115e-06 4.7683716e-06;
	setAttr ".tk[1846:2011]" -7.6293945e-06 -1.4305115e-06 2.3841858e-06 0 -1.4305115e-06
		 7.1525574e-06 -3.3378601e-06 -1.6689301e-06 1.9073486e-06 0 -1.6689301e-06 1.9073486e-06
		 9.5367432e-07 -1.6689301e-06 -4.7683716e-07 -4.7683716e-07 -1.6689301e-06 3.8146973e-06
		 -7.6293945e-06 -1.6689301e-06 3.5762787e-06 -9.5367432e-07 1.1920929e-06 2.3841858e-06
		 3.3378601e-06 -1.6689301e-06 -8.3446503e-07 -4.2915344e-06 -1.6689301e-06 -1.4901161e-07
		 -9.5367432e-07 0 3.8743019e-07 4.7683716e-07 -1.6689301e-06 -2.2649765e-06 2.6226044e-06
		 -4.0531158e-06 -2.6226044e-06 4.7683716e-07 -1.6689301e-06 3.8146973e-06 8.3446503e-07
		 -1.6689301e-06 4.7683716e-06 0 -1.6689301e-06 6.1988831e-06 1.4305115e-06 -1.6689301e-06
		 5.2452087e-06 -1.1920929e-06 -3.0994415e-06 -4.2915344e-06 2.8610229e-06 -1.6689301e-06
		 2.3841858e-06 5.7220459e-06 -1.6689301e-06 6.6757202e-06 4.7683716e-06 -1.6689301e-06
		 -2.8610229e-06 -8.1062317e-06 -1.6689301e-06 -4.2915344e-06 0 -1.6689301e-06 4.2915344e-06
		 5.7220459e-06 -9.5367432e-07 2.8610229e-06 -1.9073486e-06 -9.5367432e-07 3.8146973e-06
		 9.5367432e-06 -9.5367432e-07 -2.3841858e-06 2.8610229e-06 -9.5367432e-07 -3.3378601e-06
		 -2.3841858e-06 -9.5367432e-07 -7.1525574e-07 3.3378601e-06 -1.1920929e-07 1.6689301e-06
		 -1.9073486e-06 -9.5367432e-07 1.013279e-06 -1.4305115e-06 -9.5367432e-07 2.682209e-07
		 -1.9073486e-06 -1.1920929e-07 4.1723251e-07 0 -9.5367432e-07 -1.1920929e-06 -4.0531158e-06
		 -2.3841858e-07 5.0067902e-06 1.4305115e-06 -9.5367432e-07 -4.7683716e-07 1.6689301e-06
		 -9.5367432e-07 -4.2915344e-06 -5.9604645e-07 -9.5367432e-07 -3.3378601e-06 8.3446503e-07
		 -9.5367432e-07 9.5367432e-06 -1.4305115e-06 7.1525574e-07 3.8146973e-06 -1.6689301e-06
		 -9.5367432e-07 6.6757202e-06 0 -9.5367432e-07 7.6293945e-06 5.7220459e-06 -9.5367432e-07
		 -6.6757202e-06 8.1062317e-06 -9.5367432e-07 0 0 -9.5367432e-07 -2.8610229e-06 -9.5367432e-06
		 6.2527761e-13 2.8610229e-06 -1.9073486e-06 6.2527761e-13 -8.5830688e-06 0 6.2527761e-13
		 -3.3378601e-06 -4.2915344e-06 6.2527761e-13 3.8146973e-06 3.3378601e-06 6.2527761e-13
		 7.1525574e-07 3.3378601e-06 -1.7053026e-13 -2.1457672e-06 3.3378601e-06 6.2527761e-13
		 5.364418e-07 -7.1525574e-06 6.2527761e-13 1.7881393e-07 1.9073486e-06 -3.9790393e-13
		 -3.5762787e-07 4.7683716e-06 6.2527761e-13 -1.1920929e-06 9.5367432e-07 -1.080025e-12
		 0 -1.4305115e-06 6.2527761e-13 -2.8610229e-06 -1.6689301e-06 6.2527761e-13 4.2915344e-06
		 4.7683716e-07 6.2527761e-13 3.3378601e-06 3.5762787e-07 6.2527761e-13 4.7683716e-06
		 -2.8610229e-06 1.080025e-12 1.9073486e-06 -3.0994415e-06 6.2527761e-13 -8.5830688e-06
		 4.7683716e-06 6.2527761e-13 -2.3841858e-06 6.1988831e-06 6.2527761e-13 -2.8610229e-06
		 -8.5830688e-06 6.2527761e-13 -2.8610229e-06 0 6.2527761e-13 -9.5367432e-07 5.7220459e-06
		 1.1920929e-07 2.8610229e-06 -1.9073486e-06 1.1920929e-07 3.8146973e-06 9.5367432e-06
		 1.1920929e-07 -2.3841858e-06 2.8610229e-06 1.1920929e-07 -3.3378601e-06 -2.3841858e-06
		 1.1920929e-07 -7.1525574e-07 3.3378601e-06 7.1525574e-07 1.6689301e-06 -1.9073486e-06
		 1.1920929e-07 1.013279e-06 -1.4305115e-06 1.1920929e-07 2.682209e-07 -1.9073486e-06
		 -5.9604645e-07 4.1723251e-07 0 1.1920929e-07 -1.1920929e-06 -4.0531158e-06 -7.1525574e-07
		 5.0067902e-06 1.4305115e-06 1.1920929e-07 -4.7683716e-07 1.6689301e-06 1.1920929e-07
		 -4.2915344e-06 -5.9604645e-07 1.1920929e-07 -3.3378601e-06 8.3446503e-07 1.1920929e-07
		 9.5367432e-06 -1.4305115e-06 -1.4305115e-06 3.8146973e-06 -1.6689301e-06 1.1920929e-07
		 6.6757202e-06 0 1.1920929e-07 7.6293945e-06 5.7220459e-06 1.1920929e-07 -6.6757202e-06
		 8.1062317e-06 1.1920929e-07 0 0 1.1920929e-07 -2.8610229e-06 -3.3378601e-06 1.4305115e-06
		 1.9073486e-06 0 1.4305115e-06 1.9073486e-06 9.5367432e-07 1.4305115e-06 -4.7683716e-07
		 -4.7683716e-07 1.4305115e-06 3.8146973e-06 -7.6293945e-06 1.4305115e-06 3.5762787e-06
		 -9.5367432e-07 -1.9073486e-06 2.3841858e-06 3.3378601e-06 1.4305115e-06 -8.3446503e-07
		 -4.2915344e-06 1.4305115e-06 -1.4901161e-07 -9.5367432e-07 -7.1525574e-07 3.8743019e-07
		 4.7683716e-07 1.4305115e-06 -2.2649765e-06 2.6226044e-06 3.3378601e-06 -2.6226044e-06
		 4.7683716e-07 1.4305115e-06 3.8146973e-06 8.3446503e-07 1.4305115e-06 4.7683716e-06
		 0 1.4305115e-06 6.1988831e-06 1.4305115e-06 1.4305115e-06 5.2452087e-06 -1.1920929e-06
		 2.1457672e-06 -4.2915344e-06 2.8610229e-06 1.4305115e-06 2.3841858e-06 5.7220459e-06
		 1.4305115e-06 6.6757202e-06 4.7683716e-06 1.4305115e-06 -2.8610229e-06 -8.1062317e-06
		 1.4305115e-06 -4.2915344e-06 0 1.4305115e-06 4.2915344e-06 -6.6757202e-06 1.4305115e-06
		 1.9073486e-06 2.3841858e-06 1.4305115e-06 7.6293945e-06 4.7683716e-07 1.4305115e-06
		 4.7683716e-07 2.3841858e-06 1.4305115e-06 -4.7683716e-07 5.7220459e-06 1.4305115e-06
		 4.7683716e-06 -1.4305115e-06 1.4305115e-06 3.5762787e-06 4.7683716e-07 1.4305115e-06
		 8.3446503e-07 4.2915344e-06 1.4305115e-06 -5.364418e-07 3.5762787e-06 2.8610229e-06
		 2.9802322e-08 4.7683716e-07 1.4305115e-06 7.1525574e-07 7.1525574e-07 3.3378601e-06
		 2.3841858e-07 -7.1525574e-07 1.4305115e-06 2.6226044e-06 -1.7881393e-07 1.4305115e-06
		 -3.3378601e-06 2.0861626e-07 1.4305115e-06 1.9073486e-06 -1.1920929e-06 1.4305115e-06
		 5.7220459e-06 4.529953e-06 -1.9073486e-06 -4.7683716e-07 2.8610229e-06 1.4305115e-06
		 -1.4305115e-06 -6.1988831e-06 1.4305115e-06 4.7683716e-06 -1.9073486e-06 1.4305115e-06
		 4.2915344e-06 -7.6293945e-06 1.4305115e-06 1.9073486e-06 0 1.4305115e-06 7.1525574e-06
		 -6.6757202e-06 -9.5367432e-07 1.9073486e-06 -2.3841858e-06 -9.5367432e-07 -1.0490417e-05
		 -2.3841858e-06 -9.5367432e-07 6.6757202e-06 3.8146973e-06 -9.5367432e-07 -2.8610229e-06
		 -4.2915344e-06 -9.5367432e-07 1.4305115e-06 -9.5367432e-07 1.1920929e-06 -1.4305115e-06
		 4.7683716e-07 -9.5367432e-07 8.3446503e-07 -3.3378601e-06 -9.5367432e-07 7.1525574e-07
		 2.1457672e-06 2.6226044e-06 2.9802322e-07 1.9073486e-06 -9.5367432e-07 -3.2782555e-07
		 4.7683716e-07 -2.8610229e-06 1.1920929e-06 -8.9406967e-08 -9.5367432e-07 1.4305115e-06
		 2.9802322e-08 -9.5367432e-07 2.6226044e-06 -1.1920929e-07 -9.5367432e-07 -4.0531158e-06
		 -1.4305115e-06 -9.5367432e-07 9.5367432e-07 -3.8146973e-06 -2.3841858e-06 -5.2452087e-06
		 -2.8610229e-06 -9.5367432e-07 6.6757202e-06 -3.8146973e-06 -9.5367432e-07 6.1988831e-06
		 6.6757202e-06 -9.5367432e-07 -8.1062317e-06 -7.6293945e-06 -9.5367432e-07 2.3841858e-06
		 0 -9.5367432e-07 -4.7683716e-06 2.3841858e-06 -4.7683716e-07 1.0490417e-05 1.9073486e-06
		 -4.7683716e-07 2.8610229e-06 2.3841858e-06 -4.7683716e-07 -9.5367432e-07 5.2452087e-06
		 -4.7683716e-07 4.2915344e-06 3.0994415e-06 -4.7683716e-07 -1.9073486e-06 -4.0531158e-06
		 2.3841858e-07 -1.9073486e-06 3.0994415e-06 -4.7683716e-07 2.3841858e-06 1.1920929e-06
		 -4.7683716e-07 1.4305115e-06 -4.7683716e-07 4.7683716e-06 0 0 -4.7683716e-07 -3.2782555e-07
		 2.0861626e-07 -4.2915344e-06 -1.7881393e-07 5.6624413e-07 -4.7683716e-07 -3.5762787e-07
		 2.9802322e-07 -4.7683716e-07 -7.1525574e-07 -1.1920929e-07 -4.7683716e-07 -2.6226044e-06
		 1.1920929e-06 -4.7683716e-07 2.3841858e-06 -4.2915344e-06 -1.9073486e-06 1.4305115e-06
		 -4.7683716e-07 -4.7683716e-07 3.5762787e-06;
	setAttr ".tk[2012:2177]" -3.3378601e-06 -4.7683716e-07 1.9073486e-06 1.9073486e-06
		 -4.7683716e-07 1.4305115e-06 2.8610229e-06 -4.7683716e-07 -3.3378601e-06 0 -4.7683716e-07
		 -4.2915344e-06 -4.0531158e-06 -4.7683716e-07 9.5367432e-06 0 -4.7683716e-07 -6.6757202e-06
		 -1.9073486e-06 -4.7683716e-07 1.001358e-05 2.6226044e-06 -4.7683716e-07 -7.6293945e-06
		 1.4305115e-06 -4.7683716e-07 -2.8610229e-06 2.3841858e-07 1.4305115e-06 -2.3841858e-06
		 5.9604645e-07 -4.7683716e-07 -4.0531158e-06 -9.5367432e-07 -4.7683716e-07 9.5367432e-07
		 2.9802322e-07 2.6226044e-06 2.5033951e-06 1.8626451e-09 -4.7683716e-07 -8.3446503e-07
		 1.1920929e-07 -2.8610229e-06 -5.9604645e-08 1.1920929e-06 -4.7683716e-07 -1.4901161e-08
		 -5.9604645e-07 -4.7683716e-07 5.9604645e-07 1.1920929e-06 -4.7683716e-07 -2.2649765e-06
		 -3.0994415e-06 -4.7683716e-07 -1.5497208e-06 4.7683716e-07 -2.3841858e-06 3.5762787e-07
		 -1.9073486e-06 -4.7683716e-07 -1.1920929e-06 7.6293945e-06 -4.7683716e-07 -7.1525574e-07
		 -1.0490417e-05 -4.7683716e-07 1.6689301e-06 -1.0490417e-05 -4.7683716e-07 -1.6689301e-06
		 0 -4.7683716e-07 2.6226044e-06 3.5762787e-07 1.4305115e-06 1.0490417e-05 5.9604645e-07
		 1.4305115e-06 -7.6293945e-06 1.7881393e-06 1.4305115e-06 3.8146973e-06 -9.5367432e-07
		 1.4305115e-06 -6.1988831e-06 -1.1920929e-07 1.4305115e-06 7.1525574e-06 -1.1920929e-07
		 1.4305115e-06 -9.5367432e-07 1.7881393e-07 1.4305115e-06 1.9073486e-06 3.7252903e-08
		 1.4305115e-06 1.1920929e-06 7.4505806e-08 3.0994415e-06 7.1525574e-07 -1.0728836e-06
		 1.4305115e-06 9.5367432e-07 -1.1920929e-06 3.8146973e-06 1.0728836e-06 2.3841858e-07
		 1.4305115e-06 4.1723251e-07 1.9073486e-06 1.4305115e-06 -4.1723251e-07 1.6689301e-06
		 1.4305115e-06 1.4901161e-07 4.2915344e-06 1.4305115e-06 5.364418e-07 -3.8146973e-06
		 -1.4305115e-06 -4.1723251e-07 2.3841858e-06 1.4305115e-06 5.9604645e-07 1.4305115e-06
		 1.4305115e-06 -9.5367432e-07 -4.2915344e-06 1.4305115e-06 2.3841858e-07 6.6757202e-06
		 1.4305115e-06 2.1457672e-06 0 1.4305115e-06 1.0728836e-06 -2.9802322e-07 1.6689301e-06
		 1.0490417e-05 4.1723251e-07 1.6689301e-06 6.6757202e-06 -5.364418e-07 1.6689301e-06
		 -5.7220459e-06 2.3841858e-07 1.6689301e-06 -2.8610229e-06 -1.7881393e-07 1.6689301e-06
		 8.5830688e-06 5.364418e-07 -1.4305115e-06 -3.3378601e-06 2.3841858e-07 1.6689301e-06
		 2.3841858e-06 1.7881393e-07 1.6689301e-06 -1.9073486e-06 1.0728836e-06 -4.7683716e-07
		 -3.0994415e-06 -1.1920929e-06 1.6689301e-06 7.1525574e-07 -1.6689301e-06 3.8146973e-06
		 2.3841858e-07 2.6226044e-06 1.6689301e-06 8.3446503e-07 -2.8610229e-06 1.6689301e-06
		 -3.5762787e-07 -2.8610229e-06 1.6689301e-06 -1.1920929e-07 -1.9073486e-06 1.6689301e-06
		 -2.9802322e-08 -3.8146973e-06 2.6226044e-06 2.0861626e-07 4.7683716e-07 1.6689301e-06
		 -2.9802322e-07 7.1525574e-06 1.6689301e-06 0 0 1.6689301e-06 -1.4901161e-08 4.7683716e-06
		 1.6689301e-06 -4.7683716e-07 0 1.6689301e-06 3.5762787e-07 1.1920929e-07 4.7683716e-07
		 1.0490417e-05 -1.4901161e-08 4.7683716e-07 -8.5830688e-06 -2.3841858e-07 4.7683716e-07
		 -4.7683716e-06 8.9406967e-08 4.7683716e-07 6.1988831e-06 2.682209e-07 4.7683716e-07
		 4.7683716e-07 -1.4901161e-07 1.0728836e-06 2.8610229e-06 -2.0265579e-06 4.7683716e-07
		 -1.9073486e-06 7.1525574e-07 4.7683716e-07 -4.7683716e-07 1.1920929e-07 -2.3841858e-07
		 -3.8146973e-06 -9.5367432e-07 4.7683716e-07 -7.1525574e-07 -3.8146973e-06 -4.7683716e-07
		 1.4305115e-06 2.1457672e-06 4.7683716e-07 1.1920929e-06 -2.8610229e-06 4.7683716e-07
		 9.5367432e-07 6.1988831e-06 4.7683716e-07 -2.3841858e-07 1.9073486e-06 4.7683716e-07
		 -5.9604645e-08 -4.2915344e-06 -1.0728836e-06 -7.4505806e-08 -1.4305115e-06 4.7683716e-07
		 -1.4901161e-08 3.8146973e-06 4.7683716e-07 7.7486038e-07 -1.4305115e-06 4.7683716e-07
		 -2.3841858e-07 8.5830688e-06 4.7683716e-07 -1.4901161e-08 0 4.7683716e-07 0 -1.4901161e-08
		 6.2527761e-13 5.7220459e-06 -7.4505806e-08 -8.3446503e-07 -8.5830688e-06 5.9604645e-08
		 -8.3446503e-07 1.0490417e-05 0 6.2527761e-13 9.5367432e-06 3.7252903e-08 6.2527761e-13
		 -4.2915344e-06 2.0861626e-07 -8.3446503e-07 -5.2452087e-06 5.9604645e-08 6.2527761e-13
		 -2.3841858e-06 -1.7881393e-07 -8.3446503e-07 5.2452087e-06 4.7683716e-07 6.2527761e-13
		 -1.9073486e-06 -2.9802322e-08 -8.3446503e-07 -4.7683716e-07 -2.9802322e-08 -1.7053026e-13
		 -2.8610229e-06 -4.4703484e-08 -1.1920929e-06 2.3841858e-06 9.5367432e-07 6.2527761e-13
		 -1.4305115e-06 -1.3113022e-06 -8.3446503e-07 -2.8610229e-06 -1.3113022e-06 6.2527761e-13
		 -5.2452087e-06 -1.1920929e-07 -8.3446503e-07 -4.7683716e-07 -8.3446503e-07 -3.9790393e-13
		 7.1525574e-07 -9.5367432e-07 0 -5.0067902e-06 -1.4305115e-06 6.2527761e-13 1.4305115e-06
		 0 -8.3446503e-07 -1.9073486e-06 -2.3841858e-06 -1.080025e-12 2.1457672e-06 -2.3841858e-06
		 0 2.3841858e-07 3.5762787e-06 6.2527761e-13 4.7683716e-07 2.8610229e-06 -8.3446503e-07
		 7.1525574e-07 -9.5367432e-07 6.2527761e-13 1.9073486e-06 -1.9073486e-06 -8.3446503e-07
		 2.3841858e-06 2.8610229e-06 6.2527761e-13 3.5762787e-07 -9.5367432e-07 -8.3446503e-07
		 -1.1920929e-06 1.9073486e-06 6.2527761e-13 -1.1920929e-06 2.8610229e-06 -8.3446503e-07
		 5.9604645e-08 -9.5367432e-07 1.080025e-12 -2.0861626e-07 -3.3378601e-06 8.3446503e-07
		 5.9604645e-08 4.7683716e-07 6.2527761e-13 4.1723251e-07 -9.5367432e-07 -8.3446503e-07
		 -1.6391277e-07 8.5830688e-06 6.2527761e-13 9.5367432e-07 3.8146973e-06 -8.3446503e-07
		 -3.5762787e-07 1.0490417e-05 6.2527761e-13 4.7683716e-07 -9.5367432e-07 -8.3446503e-07
		 2.9802322e-07 -5.7220459e-06 6.2527761e-13 -3.7252903e-09 8.5830688e-06 -8.3446503e-07
		 5.9604645e-08 0 6.2527761e-13 2.7939677e-09 0 -8.3446503e-07 7.4505806e-08 -4.1723251e-07
		 -1.6689301e-06 6.6757202e-06 -2.9802322e-07 -1.6689301e-06 1.0490417e-05 1.013279e-06
		 -1.6689301e-06 -6.1988831e-06 -8.9406967e-08 -1.6689301e-06 -3.3378601e-06 5.9604645e-08
		 -1.6689301e-06 8.5830688e-06 7.1525574e-07 1.4305115e-06 -4.2915344e-06 -5.9604645e-07
		 -1.6689301e-06 1.4305115e-06 -7.1525574e-07 -1.6689301e-06 -2.8610229e-06 7.7486038e-07
		 0 -4.0531158e-06 -5.9604645e-07 -1.6689301e-06 1.4305115e-06 -1.9073486e-06 -4.0531158e-06
		 1.9073486e-06 1.9073486e-06 -1.6689301e-06 -8.3446503e-07 -1.9073486e-06 -1.6689301e-06
		 -1.1920929e-06 -1.9073486e-06 -1.6689301e-06 -9.5367432e-07 -1.4305115e-06 -1.6689301e-06
		 -8.9406967e-08 -3.3378601e-06 -2.8610229e-06 1.4901161e-08 1.4305115e-06 -1.6689301e-06
		 1.4901161e-07 7.6293945e-06 -1.6689301e-06 -7.4505806e-09 0 -1.6689301e-06 7.4505806e-08
		 4.7683716e-06 -1.6689301e-06 1.7881393e-07 0 -1.6689301e-06 -1.1920929e-07 5.9604645e-07
		 -1.4305115e-06 -7.6293945e-06 1.1920929e-06 -1.4305115e-06 1.0490417e-05 1.1920929e-07
		 -1.4305115e-06 3.3378601e-06 1.1920929e-07 -1.4305115e-06 -6.6757202e-06 -1.1920929e-07
		 -1.4305115e-06 6.6757202e-06 4.7683716e-07 -1.6689301e-06 -1.4305115e-06 -2.682209e-07
		 -1.4305115e-06 1.4305115e-06 1.4901161e-08 -1.4305115e-06 4.7683716e-07 -5.2154064e-08
		 -3.0994415e-06 -2.3841858e-07 -4.1723251e-07 -1.4305115e-06 4.7683716e-07 -8.3446503e-07
		 -3.8146973e-06 2.3841858e-07 1.1920929e-06 -1.4305115e-06 -5.364418e-07 1.1920929e-06
		 -1.4305115e-06 -5.9604645e-07 1.9073486e-06 -1.4305115e-06 7.4505806e-08 4.7683716e-06
		 -1.4305115e-06 1.7881393e-07;
	setAttr ".tk[2178:2343]" -3.8146973e-06 1.6689301e-06 -9.5367432e-07 3.3378601e-06
		 -1.4305115e-06 -7.1525574e-07 -1.001358e-05 -1.4305115e-06 -1.3113022e-06 -4.2915344e-06
		 -1.4305115e-06 2.0265579e-06 6.6757202e-06 -1.4305115e-06 1.3113022e-06 0 -1.4305115e-06
		 2.3841858e-07 4.7683716e-07 9.5367432e-07 -6.6757202e-06 -3.8146973e-06 9.5367432e-07
		 9.5367432e-06 -1.6689301e-06 9.5367432e-07 9.5367432e-06 1.4305115e-06 9.5367432e-07
		 -8.1062317e-06 1.6689301e-06 9.5367432e-07 -2.8610229e-06 -2.6226044e-06 -1.1920929e-06
		 -2.3841858e-06 9.5367432e-07 9.5367432e-07 -4.0531158e-06 -9.5367432e-07 9.5367432e-07
		 4.7683716e-07 6.5565109e-07 -2.6226044e-06 2.0265579e-06 1.8626451e-09 9.5367432e-07
		 -1.0728836e-06 -1.0728836e-06 2.8610229e-06 0 1.4305115e-06 9.5367432e-07 4.4703484e-08
		 -9.5367432e-07 9.5367432e-07 1.1920929e-07 1.1920929e-06 9.5367432e-07 0 -2.8610229e-06
		 9.5367432e-07 -1.9073486e-06 4.7683716e-07 2.3841858e-06 -4.7683716e-07 -1.9073486e-06
		 9.5367432e-07 -1.6689301e-06 7.6293945e-06 9.5367432e-07 -1.4305115e-06 -1.0490417e-05
		 9.5367432e-07 2.6226044e-06 -1.0490417e-05 9.5367432e-07 -1.6689301e-06 0 9.5367432e-07
		 2.1457672e-06 1.9073486e-06 4.7683716e-07 2.8610229e-06 2.3841858e-06 4.7683716e-07
		 1.0490417e-05 2.3841858e-06 4.7683716e-07 -9.5367432e-07 5.2452087e-06 4.7683716e-07
		 4.2915344e-06 3.0994415e-06 4.7683716e-07 -1.9073486e-06 -4.0531158e-06 -2.3841858e-07
		 -1.9073486e-06 3.0994415e-06 4.7683716e-07 2.3841858e-06 1.1920929e-06 4.7683716e-07
		 1.4305115e-06 -4.7683716e-07 -4.7683716e-06 0 0 4.7683716e-07 -3.2782555e-07 2.0861626e-07
		 4.2915344e-06 -1.7881393e-07 5.6624413e-07 4.7683716e-07 -3.5762787e-07 2.9802322e-07
		 4.7683716e-07 -7.1525574e-07 -1.1920929e-07 4.7683716e-07 -2.6226044e-06 1.1920929e-06
		 4.7683716e-07 2.3841858e-06 -4.2915344e-06 1.9073486e-06 1.4305115e-06 -4.7683716e-07
		 4.7683716e-07 3.5762787e-06 -3.3378601e-06 4.7683716e-07 1.9073486e-06 1.9073486e-06
		 4.7683716e-07 1.4305115e-06 2.8610229e-06 4.7683716e-07 -3.3378601e-06 0 4.7683716e-07
		 -4.2915344e-06 -2.3841858e-06 4.7683716e-07 -1.0490417e-05 -6.6757202e-06 4.7683716e-07
		 1.9073486e-06 -2.3841858e-06 4.7683716e-07 6.6757202e-06 3.8146973e-06 4.7683716e-07
		 -2.8610229e-06 -4.2915344e-06 4.7683716e-07 1.4305115e-06 -9.5367432e-07 -1.4305115e-06
		 -1.4305115e-06 4.7683716e-07 4.7683716e-07 8.3446503e-07 -3.3378601e-06 4.7683716e-07
		 7.1525574e-07 2.1457672e-06 -2.6226044e-06 2.9802322e-07 1.9073486e-06 4.7683716e-07
		 -3.2782555e-07 4.7683716e-07 2.3841858e-06 1.1920929e-06 -8.9406967e-08 4.7683716e-07
		 1.4305115e-06 2.9802322e-08 4.7683716e-07 2.6226044e-06 -1.1920929e-07 4.7683716e-07
		 -4.0531158e-06 -1.4305115e-06 4.7683716e-07 9.5367432e-07 -3.8146973e-06 2.3841858e-06
		 -5.2452087e-06 -2.8610229e-06 4.7683716e-07 6.6757202e-06 -3.8146973e-06 4.7683716e-07
		 6.1988831e-06 6.6757202e-06 4.7683716e-07 -8.1062317e-06 -7.6293945e-06 4.7683716e-07
		 2.3841858e-06 0 4.7683716e-07 -4.7683716e-06 1.4305115e-06 -1.4305115e-06 7.6293945e-06
		 -4.7683716e-06 -1.4305115e-06 1.9073486e-06 -4.7683716e-07 -1.4305115e-06 4.7683716e-07
		 1.9073486e-06 -1.4305115e-06 -4.7683716e-06 5.2452087e-06 -1.4305115e-06 5.2452087e-06
		 -1.4305115e-06 -1.4305115e-06 3.8146973e-06 0 -1.4305115e-06 1.0728836e-06 4.2915344e-06
		 -1.4305115e-06 -3.5762787e-07 2.6226044e-06 -3.0994415e-06 1.4901161e-08 4.7683716e-07
		 -1.4305115e-06 9.5367432e-07 -4.7683716e-07 -3.3378601e-06 2.3841858e-07 -7.1525574e-07
		 -1.4305115e-06 3.3378601e-06 -1.7881393e-07 -1.4305115e-06 -2.8610229e-06 4.7683716e-07
		 -1.4305115e-06 2.3841858e-06 -1.5497208e-06 -1.4305115e-06 6.6757202e-06 4.2915344e-06
		 1.4305115e-06 4.7683716e-07 2.8610229e-06 -1.4305115e-06 -1.4305115e-06 -6.6757202e-06
		 -1.4305115e-06 5.2452087e-06 -3.3378601e-06 -1.4305115e-06 4.7683716e-06 -7.6293945e-06
		 -1.4305115e-06 2.3841858e-06 0 -1.4305115e-06 7.1525574e-06 0 -1.6689301e-06 1.9073486e-06
		 -3.3378601e-06 -1.6689301e-06 1.9073486e-06 9.5367432e-07 -1.6689301e-06 -4.7683716e-07
		 -4.7683716e-07 -1.6689301e-06 3.8146973e-06 -7.6293945e-06 -1.6689301e-06 3.5762787e-06
		 -9.5367432e-07 1.1920929e-06 2.3841858e-06 3.3378601e-06 -1.6689301e-06 -8.3446503e-07
		 -4.2915344e-06 -1.6689301e-06 -1.4901161e-07 -9.5367432e-07 0 3.8743019e-07 4.7683716e-07
		 -1.6689301e-06 -2.2649765e-06 2.6226044e-06 -4.0531158e-06 -2.6226044e-06 4.7683716e-07
		 -1.6689301e-06 3.8146973e-06 8.3446503e-07 -1.6689301e-06 4.7683716e-06 0 -1.6689301e-06
		 6.1988831e-06 1.4305115e-06 -1.6689301e-06 5.2452087e-06 -1.1920929e-06 -3.0994415e-06
		 -4.2915344e-06 2.8610229e-06 -1.6689301e-06 2.3841858e-06 5.7220459e-06 -1.6689301e-06
		 6.6757202e-06 4.7683716e-06 -1.6689301e-06 -2.8610229e-06 -8.1062317e-06 -1.6689301e-06
		 -4.2915344e-06 0 -1.6689301e-06 4.2915344e-06 -1.9073486e-06 -9.5367432e-07 3.8146973e-06
		 5.7220459e-06 -9.5367432e-07 2.8610229e-06 9.5367432e-06 -9.5367432e-07 -2.3841858e-06
		 2.8610229e-06 -9.5367432e-07 -3.3378601e-06 -2.3841858e-06 -9.5367432e-07 -7.1525574e-07
		 3.3378601e-06 -1.1920929e-07 1.6689301e-06 -1.9073486e-06 -9.5367432e-07 1.013279e-06
		 -1.4305115e-06 -9.5367432e-07 2.682209e-07 -1.9073486e-06 -1.1920929e-07 4.1723251e-07
		 0 -9.5367432e-07 -1.1920929e-06 -4.0531158e-06 -2.3841858e-07 5.0067902e-06 1.4305115e-06
		 -9.5367432e-07 -4.7683716e-07 1.6689301e-06 -9.5367432e-07 -4.2915344e-06 -5.9604645e-07
		 -9.5367432e-07 -3.3378601e-06 8.3446503e-07 -9.5367432e-07 9.5367432e-06 -1.4305115e-06
		 7.1525574e-07 3.8146973e-06 -1.6689301e-06 -9.5367432e-07 6.6757202e-06 0 -9.5367432e-07
		 7.6293945e-06 5.7220459e-06 -9.5367432e-07 -6.6757202e-06 8.1062317e-06 -9.5367432e-07
		 0 0 -9.5367432e-07 -2.8610229e-06 -1.9073486e-06 6.2527761e-13 -8.5830688e-06 -9.5367432e-06
		 6.2527761e-13 2.8610229e-06 0 6.2527761e-13 -3.3378601e-06 -4.2915344e-06 6.2527761e-13
		 3.8146973e-06 3.3378601e-06 6.2527761e-13 7.1525574e-07 3.3378601e-06 -1.7053026e-13
		 -2.1457672e-06 3.3378601e-06 6.2527761e-13 5.364418e-07 -7.1525574e-06 6.2527761e-13
		 1.7881393e-07 1.9073486e-06 -3.9790393e-13 -3.5762787e-07 4.7683716e-06 6.2527761e-13
		 -1.1920929e-06 9.5367432e-07 -1.080025e-12 0 -1.4305115e-06 6.2527761e-13 -2.8610229e-06
		 -1.6689301e-06 6.2527761e-13 4.2915344e-06 4.7683716e-07 6.2527761e-13 3.3378601e-06
		 3.5762787e-07 6.2527761e-13 4.7683716e-06 -2.8610229e-06 1.080025e-12 1.9073486e-06
		 -3.0994415e-06 6.2527761e-13 -8.5830688e-06 4.7683716e-06 6.2527761e-13 -2.3841858e-06
		 6.1988831e-06 6.2527761e-13 -2.8610229e-06 -8.5830688e-06 6.2527761e-13 -2.8610229e-06
		 0 6.2527761e-13 -9.5367432e-07 -1.9073486e-06 1.1920929e-07 3.8146973e-06 5.7220459e-06
		 1.1920929e-07 2.8610229e-06 9.5367432e-06 1.1920929e-07 -2.3841858e-06 2.8610229e-06
		 1.1920929e-07 -3.3378601e-06 -2.3841858e-06 1.1920929e-07 -7.1525574e-07 3.3378601e-06
		 7.1525574e-07 1.6689301e-06 -1.9073486e-06 1.1920929e-07 1.013279e-06 -1.4305115e-06
		 1.1920929e-07 2.682209e-07 -1.9073486e-06 -5.9604645e-07 4.1723251e-07 0 1.1920929e-07
		 -1.1920929e-06 -4.0531158e-06 -7.1525574e-07 5.0067902e-06 1.4305115e-06 1.1920929e-07
		 -4.7683716e-07 1.6689301e-06 1.1920929e-07 -4.2915344e-06;
	setAttr ".tk[2344:2509]" -5.9604645e-07 1.1920929e-07 -3.3378601e-06 8.3446503e-07
		 1.1920929e-07 9.5367432e-06 -1.4305115e-06 -1.4305115e-06 3.8146973e-06 -1.6689301e-06
		 1.1920929e-07 6.6757202e-06 0 1.1920929e-07 7.6293945e-06 5.7220459e-06 1.1920929e-07
		 -6.6757202e-06 8.1062317e-06 1.1920929e-07 0 0 1.1920929e-07 -2.8610229e-06 0 1.4305115e-06
		 1.9073486e-06 -3.3378601e-06 1.4305115e-06 1.9073486e-06 9.5367432e-07 1.4305115e-06
		 -4.7683716e-07 -4.7683716e-07 1.4305115e-06 3.8146973e-06 -7.6293945e-06 1.4305115e-06
		 3.5762787e-06 -9.5367432e-07 -1.9073486e-06 2.3841858e-06 3.3378601e-06 1.4305115e-06
		 -8.3446503e-07 -4.2915344e-06 1.4305115e-06 -1.4901161e-07 -9.5367432e-07 -7.1525574e-07
		 3.8743019e-07 4.7683716e-07 1.4305115e-06 -2.2649765e-06 2.6226044e-06 3.3378601e-06
		 -2.6226044e-06 4.7683716e-07 1.4305115e-06 3.8146973e-06 8.3446503e-07 1.4305115e-06
		 4.7683716e-06 0 1.4305115e-06 6.1988831e-06 1.4305115e-06 1.4305115e-06 5.2452087e-06
		 -1.1920929e-06 2.1457672e-06 -4.2915344e-06 2.8610229e-06 1.4305115e-06 2.3841858e-06
		 5.7220459e-06 1.4305115e-06 6.6757202e-06 4.7683716e-06 1.4305115e-06 -2.8610229e-06
		 -8.1062317e-06 1.4305115e-06 -4.2915344e-06 0 1.4305115e-06 4.2915344e-06 2.3841858e-06
		 1.4305115e-06 7.6293945e-06 -6.6757202e-06 1.4305115e-06 1.9073486e-06 4.7683716e-07
		 1.4305115e-06 4.7683716e-07 2.3841858e-06 1.4305115e-06 -4.7683716e-07 5.7220459e-06
		 1.4305115e-06 4.7683716e-06 -1.4305115e-06 1.4305115e-06 3.5762787e-06 4.7683716e-07
		 1.4305115e-06 8.3446503e-07 4.2915344e-06 1.4305115e-06 -5.364418e-07 3.5762787e-06
		 2.8610229e-06 2.9802322e-08 4.7683716e-07 1.4305115e-06 7.1525574e-07 7.1525574e-07
		 3.3378601e-06 2.3841858e-07 -7.1525574e-07 1.4305115e-06 2.6226044e-06 -1.7881393e-07
		 1.4305115e-06 -3.3378601e-06 2.0861626e-07 1.4305115e-06 1.9073486e-06 -1.1920929e-06
		 1.4305115e-06 5.7220459e-06 4.529953e-06 -1.9073486e-06 -4.7683716e-07 2.8610229e-06
		 1.4305115e-06 -1.4305115e-06 -6.1988831e-06 1.4305115e-06 4.7683716e-06 -1.9073486e-06
		 1.4305115e-06 4.2915344e-06 -7.6293945e-06 1.4305115e-06 1.9073486e-06 0 1.4305115e-06
		 7.1525574e-06 -2.3841858e-06 -9.5367432e-07 -1.0490417e-05 -6.6757202e-06 -9.5367432e-07
		 1.9073486e-06 -2.3841858e-06 -9.5367432e-07 6.6757202e-06 3.8146973e-06 -9.5367432e-07
		 -2.8610229e-06 -4.2915344e-06 -9.5367432e-07 1.4305115e-06 -9.5367432e-07 1.1920929e-06
		 -1.4305115e-06 4.7683716e-07 -9.5367432e-07 8.3446503e-07 -3.3378601e-06 -9.5367432e-07
		 7.1525574e-07 2.1457672e-06 2.6226044e-06 2.9802322e-07 1.9073486e-06 -9.5367432e-07
		 -3.2782555e-07 4.7683716e-07 -2.8610229e-06 1.1920929e-06 -8.9406967e-08 -9.5367432e-07
		 1.4305115e-06 2.9802322e-08 -9.5367432e-07 2.6226044e-06 -1.1920929e-07 -9.5367432e-07
		 -4.0531158e-06 -1.4305115e-06 -9.5367432e-07 9.5367432e-07 -3.8146973e-06 -2.3841858e-06
		 -5.2452087e-06 -2.8610229e-06 -9.5367432e-07 6.6757202e-06 -3.8146973e-06 -9.5367432e-07
		 6.1988831e-06 6.6757202e-06 -9.5367432e-07 -8.1062317e-06 -7.6293945e-06 -9.5367432e-07
		 2.3841858e-06 0 -9.5367432e-07 -4.7683716e-06 1.9073486e-06 -4.7683716e-07 2.8610229e-06
		 2.3841858e-06 -4.7683716e-07 1.0490417e-05 2.3841858e-06 -4.7683716e-07 -9.5367432e-07
		 5.2452087e-06 -4.7683716e-07 4.2915344e-06 3.0994415e-06 -4.7683716e-07 -1.9073486e-06
		 -4.0531158e-06 2.3841858e-07 -1.9073486e-06 3.0994415e-06 -4.7683716e-07 2.3841858e-06
		 1.1920929e-06 -4.7683716e-07 1.4305115e-06 -4.7683716e-07 4.7683716e-06 0 0 -4.7683716e-07
		 -3.2782555e-07 2.0861626e-07 -4.2915344e-06 -1.7881393e-07 5.6624413e-07 -4.7683716e-07
		 -3.5762787e-07 2.9802322e-07 -4.7683716e-07 -7.1525574e-07 -1.1920929e-07 -4.7683716e-07
		 -2.6226044e-06 1.1920929e-06 -4.7683716e-07 2.3841858e-06 -4.2915344e-06 -1.9073486e-06
		 1.4305115e-06 -4.7683716e-07 -4.7683716e-07 3.5762787e-06 -3.3378601e-06 -4.7683716e-07
		 1.9073486e-06 1.9073486e-06 -4.7683716e-07 1.4305115e-06 2.8610229e-06 -4.7683716e-07
		 -3.3378601e-06 0 -4.7683716e-07 -4.2915344e-06 0 -4.7683716e-07 -6.6757202e-06 -4.0531158e-06
		 -4.7683716e-07 9.5367432e-06 -1.9073486e-06 -4.7683716e-07 1.001358e-05 2.6226044e-06
		 -4.7683716e-07 -7.6293945e-06 1.4305115e-06 -4.7683716e-07 -2.8610229e-06 2.3841858e-07
		 1.4305115e-06 -2.3841858e-06 5.9604645e-07 -4.7683716e-07 -4.0531158e-06 -9.5367432e-07
		 -4.7683716e-07 9.5367432e-07 2.9802322e-07 2.6226044e-06 2.5033951e-06 1.8626451e-09
		 -4.7683716e-07 -8.3446503e-07 1.1920929e-07 -2.8610229e-06 -5.9604645e-08 1.1920929e-06
		 -4.7683716e-07 -1.4901161e-08 -5.9604645e-07 -4.7683716e-07 5.9604645e-07 1.1920929e-06
		 -4.7683716e-07 -2.2649765e-06 -3.0994415e-06 -4.7683716e-07 -1.5497208e-06 4.7683716e-07
		 -2.3841858e-06 3.5762787e-07 -1.9073486e-06 -4.7683716e-07 -1.1920929e-06 7.6293945e-06
		 -4.7683716e-07 -7.1525574e-07 -1.0490417e-05 -4.7683716e-07 1.6689301e-06 -1.0490417e-05
		 -4.7683716e-07 -1.6689301e-06 0 -4.7683716e-07 2.6226044e-06 5.9604645e-07 1.4305115e-06
		 -7.6293945e-06 3.5762787e-07 1.4305115e-06 1.0490417e-05 1.7881393e-06 1.4305115e-06
		 3.8146973e-06 -9.5367432e-07 1.4305115e-06 -6.1988831e-06 -1.1920929e-07 1.4305115e-06
		 7.1525574e-06 -1.1920929e-07 1.4305115e-06 -9.5367432e-07 1.7881393e-07 1.4305115e-06
		 1.9073486e-06 3.7252903e-08 1.4305115e-06 1.1920929e-06 7.4505806e-08 3.0994415e-06
		 7.1525574e-07 -1.0728836e-06 1.4305115e-06 9.5367432e-07 -1.1920929e-06 3.8146973e-06
		 1.0728836e-06 2.3841858e-07 1.4305115e-06 4.1723251e-07 1.9073486e-06 1.4305115e-06
		 -4.1723251e-07 1.6689301e-06 1.4305115e-06 1.4901161e-07 4.2915344e-06 1.4305115e-06
		 5.364418e-07 -3.8146973e-06 -1.4305115e-06 -4.1723251e-07 2.3841858e-06 1.4305115e-06
		 5.9604645e-07 1.4305115e-06 1.4305115e-06 -9.5367432e-07 -4.2915344e-06 1.4305115e-06
		 2.3841858e-07 6.6757202e-06 1.4305115e-06 2.1457672e-06 0 1.4305115e-06 1.0728836e-06
		 4.1723251e-07 1.6689301e-06 6.6757202e-06 -2.9802322e-07 1.6689301e-06 1.0490417e-05
		 -5.364418e-07 1.6689301e-06 -5.7220459e-06 2.3841858e-07 1.6689301e-06 -2.8610229e-06
		 -1.7881393e-07 1.6689301e-06 8.5830688e-06 5.364418e-07 -1.4305115e-06 -3.3378601e-06
		 2.3841858e-07 1.6689301e-06 2.3841858e-06 1.7881393e-07 1.6689301e-06 -1.9073486e-06
		 1.0728836e-06 -4.7683716e-07 -3.0994415e-06 -1.1920929e-06 1.6689301e-06 7.1525574e-07
		 -1.6689301e-06 3.8146973e-06 2.3841858e-07 2.6226044e-06 1.6689301e-06 8.3446503e-07
		 -2.8610229e-06 1.6689301e-06 -3.5762787e-07 -2.8610229e-06 1.6689301e-06 -1.1920929e-07
		 -1.9073486e-06 1.6689301e-06 -2.9802322e-08 -3.8146973e-06 2.6226044e-06 2.0861626e-07
		 4.7683716e-07 1.6689301e-06 -2.9802322e-07 7.1525574e-06 1.6689301e-06 0 0 1.6689301e-06
		 -1.4901161e-08 4.7683716e-06 1.6689301e-06 -4.7683716e-07 0 1.6689301e-06 3.5762787e-07
		 -1.4901161e-08 4.7683716e-07 -8.5830688e-06 1.1920929e-07 4.7683716e-07 1.0490417e-05
		 -2.3841858e-07 4.7683716e-07 -4.7683716e-06 8.9406967e-08 4.7683716e-07 6.1988831e-06
		 2.682209e-07 4.7683716e-07 4.7683716e-07 -1.4901161e-07 1.0728836e-06 2.8610229e-06
		 -2.0265579e-06 4.7683716e-07 -1.9073486e-06 7.1525574e-07 4.7683716e-07 -4.7683716e-07
		 1.1920929e-07 -2.3841858e-07 -3.8146973e-06 -9.5367432e-07 4.7683716e-07 -7.1525574e-07
		 -3.8146973e-06 -4.7683716e-07 1.4305115e-06;
	setAttr ".tk[2510:2675]" 2.1457672e-06 4.7683716e-07 1.1920929e-06 -2.8610229e-06
		 4.7683716e-07 9.5367432e-07 6.1988831e-06 4.7683716e-07 -2.3841858e-07 1.9073486e-06
		 4.7683716e-07 -5.9604645e-08 -4.2915344e-06 -1.0728836e-06 -7.4505806e-08 -1.4305115e-06
		 4.7683716e-07 -1.4901161e-08 3.8146973e-06 4.7683716e-07 7.7486038e-07 -1.4305115e-06
		 4.7683716e-07 -2.3841858e-07 8.5830688e-06 4.7683716e-07 -1.4901161e-08 0 4.7683716e-07
		 0 -1.4901161e-08 6.2527761e-13 5.7220459e-06 -7.4505806e-08 -8.3446503e-07 -8.5830688e-06
		 5.9604645e-08 -8.3446503e-07 1.0490417e-05 0 6.2527761e-13 9.5367432e-06 3.7252903e-08
		 6.2527761e-13 -4.2915344e-06 2.0861626e-07 -8.3446503e-07 -5.2452087e-06 5.9604645e-08
		 6.2527761e-13 -2.3841858e-06 -1.7881393e-07 -8.3446503e-07 5.2452087e-06 4.7683716e-07
		 6.2527761e-13 -1.9073486e-06 -2.9802322e-08 -8.3446503e-07 -4.7683716e-07 -2.9802322e-08
		 -1.7053026e-13 -2.8610229e-06 -4.4703484e-08 -1.1920929e-06 2.3841858e-06 9.5367432e-07
		 6.2527761e-13 -1.4305115e-06 -1.3113022e-06 -8.3446503e-07 -2.8610229e-06 -1.3113022e-06
		 6.2527761e-13 -5.2452087e-06 -1.1920929e-07 -8.3446503e-07 -4.7683716e-07 -8.3446503e-07
		 -3.9790393e-13 7.1525574e-07 -9.5367432e-07 0 -5.0067902e-06 -1.4305115e-06 6.2527761e-13
		 1.4305115e-06 0 -8.3446503e-07 -1.9073486e-06 -2.3841858e-06 -1.080025e-12 2.1457672e-06
		 -2.3841858e-06 0 2.3841858e-07 3.5762787e-06 6.2527761e-13 4.7683716e-07 2.8610229e-06
		 -8.3446503e-07 7.1525574e-07 -9.5367432e-07 6.2527761e-13 1.9073486e-06 -1.9073486e-06
		 -8.3446503e-07 2.3841858e-06 2.8610229e-06 6.2527761e-13 3.5762787e-07 -9.5367432e-07
		 -8.3446503e-07 -1.1920929e-06 1.9073486e-06 6.2527761e-13 -1.1920929e-06 2.8610229e-06
		 -8.3446503e-07 5.9604645e-08 -9.5367432e-07 1.080025e-12 -2.0861626e-07 -3.3378601e-06
		 8.3446503e-07 5.9604645e-08 4.7683716e-07 6.2527761e-13 4.1723251e-07 -9.5367432e-07
		 -8.3446503e-07 -1.6391277e-07 8.5830688e-06 6.2527761e-13 9.5367432e-07 3.8146973e-06
		 -8.3446503e-07 -3.5762787e-07 1.0490417e-05 6.2527761e-13 4.7683716e-07 -9.5367432e-07
		 -8.3446503e-07 2.9802322e-07 -5.7220459e-06 6.2527761e-13 -3.7252903e-09 8.5830688e-06
		 -8.3446503e-07 5.9604645e-08 0 6.2527761e-13 2.7939677e-09 0 -8.3446503e-07 7.4505806e-08
		 -4.1723251e-07 -1.6689301e-06 6.6757202e-06 -2.9802322e-07 -1.6689301e-06 1.0490417e-05
		 1.013279e-06 -1.6689301e-06 -6.1988831e-06 -8.9406967e-08 -1.6689301e-06 -3.3378601e-06
		 5.9604645e-08 -1.6689301e-06 8.5830688e-06 7.1525574e-07 1.4305115e-06 -4.2915344e-06
		 -5.9604645e-07 -1.6689301e-06 1.4305115e-06 -7.1525574e-07 -1.6689301e-06 -2.8610229e-06
		 7.7486038e-07 0 -4.0531158e-06 -5.9604645e-07 -1.6689301e-06 1.4305115e-06 -1.9073486e-06
		 -4.0531158e-06 1.9073486e-06 1.9073486e-06 -1.6689301e-06 -8.3446503e-07 -1.9073486e-06
		 -1.6689301e-06 -1.1920929e-06 -1.9073486e-06 -1.6689301e-06 -9.5367432e-07 -1.4305115e-06
		 -1.6689301e-06 -8.9406967e-08 -3.3378601e-06 -2.8610229e-06 1.4901161e-08 1.4305115e-06
		 -1.6689301e-06 1.4901161e-07 7.6293945e-06 -1.6689301e-06 -7.4505806e-09 0 -1.6689301e-06
		 7.4505806e-08 4.7683716e-06 -1.6689301e-06 1.7881393e-07 0 -1.6689301e-06 -1.1920929e-07
		 5.9604645e-07 -1.4305115e-06 -7.6293945e-06 1.1920929e-06 -1.4305115e-06 1.0490417e-05
		 1.1920929e-07 -1.4305115e-06 3.3378601e-06 1.1920929e-07 -1.4305115e-06 -6.6757202e-06
		 -1.1920929e-07 -1.4305115e-06 6.6757202e-06 4.7683716e-07 -1.6689301e-06 -1.4305115e-06
		 -2.682209e-07 -1.4305115e-06 1.4305115e-06 1.4901161e-08 -1.4305115e-06 4.7683716e-07
		 -5.2154064e-08 -3.0994415e-06 -2.3841858e-07 -4.1723251e-07 -1.4305115e-06 4.7683716e-07
		 -8.3446503e-07 -3.8146973e-06 2.3841858e-07 1.1920929e-06 -1.4305115e-06 -5.364418e-07
		 1.1920929e-06 -1.4305115e-06 -5.9604645e-07 1.9073486e-06 -1.4305115e-06 7.4505806e-08
		 4.7683716e-06 -1.4305115e-06 1.7881393e-07 -3.8146973e-06 1.6689301e-06 -9.5367432e-07
		 3.3378601e-06 -1.4305115e-06 -7.1525574e-07 -1.001358e-05 -1.4305115e-06 -1.3113022e-06
		 -4.2915344e-06 -1.4305115e-06 2.0265579e-06 6.6757202e-06 -1.4305115e-06 1.3113022e-06
		 0 -1.4305115e-06 2.3841858e-07 4.7683716e-07 9.5367432e-07 -6.6757202e-06 -3.8146973e-06
		 9.5367432e-07 9.5367432e-06 -1.6689301e-06 9.5367432e-07 9.5367432e-06 1.4305115e-06
		 9.5367432e-07 -8.1062317e-06 1.6689301e-06 9.5367432e-07 -2.8610229e-06 -2.6226044e-06
		 -1.1920929e-06 -2.3841858e-06 9.5367432e-07 9.5367432e-07 -4.0531158e-06 -9.5367432e-07
		 9.5367432e-07 4.7683716e-07 6.5565109e-07 -2.6226044e-06 2.0265579e-06 1.8626451e-09
		 9.5367432e-07 -1.0728836e-06 -1.0728836e-06 2.8610229e-06 0 1.4305115e-06 9.5367432e-07
		 4.4703484e-08 -9.5367432e-07 9.5367432e-07 1.1920929e-07 1.1920929e-06 9.5367432e-07
		 0 -2.8610229e-06 9.5367432e-07 -1.9073486e-06 4.7683716e-07 2.3841858e-06 -4.7683716e-07
		 -1.9073486e-06 9.5367432e-07 -1.6689301e-06 7.6293945e-06 9.5367432e-07 -1.4305115e-06
		 -1.0490417e-05 9.5367432e-07 2.6226044e-06 -1.0490417e-05 9.5367432e-07 -1.6689301e-06
		 0 9.5367432e-07 2.1457672e-06 1.9073486e-06 4.7683716e-07 2.8610229e-06 2.3841858e-06
		 4.7683716e-07 1.0490417e-05 2.3841858e-06 4.7683716e-07 -9.5367432e-07 5.2452087e-06
		 4.7683716e-07 4.2915344e-06 3.0994415e-06 4.7683716e-07 -1.9073486e-06 -4.0531158e-06
		 -2.3841858e-07 -1.9073486e-06 3.0994415e-06 4.7683716e-07 2.3841858e-06 1.1920929e-06
		 4.7683716e-07 1.4305115e-06 -4.7683716e-07 -4.7683716e-06 0 0 4.7683716e-07 -3.2782555e-07
		 2.0861626e-07 4.2915344e-06 -1.7881393e-07 5.6624413e-07 4.7683716e-07 -3.5762787e-07
		 2.9802322e-07 4.7683716e-07 -7.1525574e-07 -1.1920929e-07 4.7683716e-07 -2.6226044e-06
		 1.1920929e-06 4.7683716e-07 2.3841858e-06 -4.2915344e-06 1.9073486e-06 1.4305115e-06
		 -4.7683716e-07 4.7683716e-07 3.5762787e-06 -3.3378601e-06 4.7683716e-07 1.9073486e-06
		 1.9073486e-06 4.7683716e-07 1.4305115e-06 2.8610229e-06 4.7683716e-07 -3.3378601e-06
		 0 4.7683716e-07 -4.2915344e-06 -2.3841858e-06 4.7683716e-07 -1.0490417e-05 -6.6757202e-06
		 4.7683716e-07 1.9073486e-06 -2.3841858e-06 4.7683716e-07 6.6757202e-06 3.8146973e-06
		 4.7683716e-07 -2.8610229e-06 -4.2915344e-06 4.7683716e-07 1.4305115e-06 -9.5367432e-07
		 -1.4305115e-06 -1.4305115e-06 4.7683716e-07 4.7683716e-07 8.3446503e-07 -3.3378601e-06
		 4.7683716e-07 7.1525574e-07 2.1457672e-06 -2.6226044e-06 2.9802322e-07 1.9073486e-06
		 4.7683716e-07 -3.2782555e-07 4.7683716e-07 2.3841858e-06 1.1920929e-06 -8.9406967e-08
		 4.7683716e-07 1.4305115e-06 2.9802322e-08 4.7683716e-07 2.6226044e-06 -1.1920929e-07
		 4.7683716e-07 -4.0531158e-06 -1.4305115e-06 4.7683716e-07 9.5367432e-07 -3.8146973e-06
		 2.3841858e-06 -5.2452087e-06 -2.8610229e-06 4.7683716e-07 6.6757202e-06 -3.8146973e-06
		 4.7683716e-07 6.1988831e-06 6.6757202e-06 4.7683716e-07 -8.1062317e-06 -7.6293945e-06
		 4.7683716e-07 2.3841858e-06 0 4.7683716e-07 -4.7683716e-06 1.4305115e-06 -1.4305115e-06
		 7.6293945e-06 -4.7683716e-06 -1.4305115e-06 1.9073486e-06 -4.7683716e-07 -1.4305115e-06
		 4.7683716e-07 1.9073486e-06 -1.4305115e-06 -4.7683716e-06 5.2452087e-06 -1.4305115e-06
		 5.2452087e-06 -1.4305115e-06 -1.4305115e-06 3.8146973e-06 0 -1.4305115e-06 1.0728836e-06
		 4.2915344e-06 -1.4305115e-06 -3.5762787e-07 2.6226044e-06 -3.0994415e-06 1.4901161e-08;
	setAttr ".tk[2676:2841]" 4.7683716e-07 -1.4305115e-06 9.5367432e-07 -4.7683716e-07
		 -3.3378601e-06 2.3841858e-07 -7.1525574e-07 -1.4305115e-06 3.3378601e-06 -1.7881393e-07
		 -1.4305115e-06 -2.8610229e-06 4.7683716e-07 -1.4305115e-06 2.3841858e-06 -1.5497208e-06
		 -1.4305115e-06 6.6757202e-06 4.2915344e-06 1.4305115e-06 4.7683716e-07 2.8610229e-06
		 -1.4305115e-06 -1.4305115e-06 -6.6757202e-06 -1.4305115e-06 5.2452087e-06 -3.3378601e-06
		 -1.4305115e-06 4.7683716e-06 -7.6293945e-06 -1.4305115e-06 2.3841858e-06 0 -1.4305115e-06
		 7.1525574e-06 0 -1.6689301e-06 1.9073486e-06 -3.3378601e-06 -1.6689301e-06 1.9073486e-06
		 9.5367432e-07 -1.6689301e-06 -4.7683716e-07 -4.7683716e-07 -1.6689301e-06 3.8146973e-06
		 -7.6293945e-06 -1.6689301e-06 3.5762787e-06 -9.5367432e-07 1.1920929e-06 2.3841858e-06
		 3.3378601e-06 -1.6689301e-06 -8.3446503e-07 -4.2915344e-06 -1.6689301e-06 -1.4901161e-07
		 -9.5367432e-07 0 3.8743019e-07 4.7683716e-07 -1.6689301e-06 -2.2649765e-06 2.6226044e-06
		 -4.0531158e-06 -2.6226044e-06 4.7683716e-07 -1.6689301e-06 3.8146973e-06 8.3446503e-07
		 -1.6689301e-06 4.7683716e-06 0 -1.6689301e-06 6.1988831e-06 1.4305115e-06 -1.6689301e-06
		 5.2452087e-06 -1.1920929e-06 -3.0994415e-06 -4.2915344e-06 2.8610229e-06 -1.6689301e-06
		 2.3841858e-06 5.7220459e-06 -1.6689301e-06 6.6757202e-06 4.7683716e-06 -1.6689301e-06
		 -2.8610229e-06 -8.1062317e-06 -1.6689301e-06 -4.2915344e-06 0 -1.6689301e-06 4.2915344e-06
		 -1.9073486e-06 -9.5367432e-07 3.8146973e-06 5.7220459e-06 -9.5367432e-07 2.8610229e-06
		 9.5367432e-06 -9.5367432e-07 -2.3841858e-06 2.8610229e-06 -9.5367432e-07 -3.3378601e-06
		 -2.3841858e-06 -9.5367432e-07 -7.1525574e-07 3.3378601e-06 -1.1920929e-07 1.6689301e-06
		 -1.9073486e-06 -9.5367432e-07 1.013279e-06 -1.4305115e-06 -9.5367432e-07 2.682209e-07
		 -1.9073486e-06 -1.1920929e-07 4.1723251e-07 0 -9.5367432e-07 -1.1920929e-06 -4.0531158e-06
		 -2.3841858e-07 5.0067902e-06 1.4305115e-06 -9.5367432e-07 -4.7683716e-07 1.6689301e-06
		 -9.5367432e-07 -4.2915344e-06 -5.9604645e-07 -9.5367432e-07 -3.3378601e-06 8.3446503e-07
		 -9.5367432e-07 9.5367432e-06 -1.4305115e-06 7.1525574e-07 3.8146973e-06 -1.6689301e-06
		 -9.5367432e-07 6.6757202e-06 0 -9.5367432e-07 7.6293945e-06 5.7220459e-06 -9.5367432e-07
		 -6.6757202e-06 8.1062317e-06 -9.5367432e-07 0 0 -9.5367432e-07 -2.8610229e-06 -1.9073486e-06
		 6.2527761e-13 -8.5830688e-06 -9.5367432e-06 6.2527761e-13 2.8610229e-06 0 6.2527761e-13
		 -3.3378601e-06 -4.2915344e-06 6.2527761e-13 3.8146973e-06 3.3378601e-06 6.2527761e-13
		 7.1525574e-07 3.3378601e-06 -1.7053026e-13 -2.1457672e-06 3.3378601e-06 6.2527761e-13
		 5.364418e-07 -7.1525574e-06 6.2527761e-13 1.7881393e-07 1.9073486e-06 -3.9790393e-13
		 -3.5762787e-07 4.7683716e-06 6.2527761e-13 -1.1920929e-06 9.5367432e-07 -1.080025e-12
		 0 -1.4305115e-06 6.2527761e-13 -2.8610229e-06 -1.6689301e-06 6.2527761e-13 4.2915344e-06
		 4.7683716e-07 6.2527761e-13 3.3378601e-06 3.5762787e-07 6.2527761e-13 4.7683716e-06
		 -2.8610229e-06 1.080025e-12 1.9073486e-06 -3.0994415e-06 6.2527761e-13 -8.5830688e-06
		 4.7683716e-06 6.2527761e-13 -2.3841858e-06 6.1988831e-06 6.2527761e-13 -2.8610229e-06
		 -8.5830688e-06 6.2527761e-13 -2.8610229e-06 0 6.2527761e-13 -9.5367432e-07 -1.9073486e-06
		 1.1920929e-07 3.8146973e-06 5.7220459e-06 1.1920929e-07 2.8610229e-06 9.5367432e-06
		 1.1920929e-07 -2.3841858e-06 2.8610229e-06 1.1920929e-07 -3.3378601e-06 -2.3841858e-06
		 1.1920929e-07 -7.1525574e-07 3.3378601e-06 7.1525574e-07 1.6689301e-06 -1.9073486e-06
		 1.1920929e-07 1.013279e-06 -1.4305115e-06 1.1920929e-07 2.682209e-07 -1.9073486e-06
		 -5.9604645e-07 4.1723251e-07 0 1.1920929e-07 -1.1920929e-06 -4.0531158e-06 -7.1525574e-07
		 5.0067902e-06 1.4305115e-06 1.1920929e-07 -4.7683716e-07 1.6689301e-06 1.1920929e-07
		 -4.2915344e-06 -5.9604645e-07 1.1920929e-07 -3.3378601e-06 8.3446503e-07 1.1920929e-07
		 9.5367432e-06 -1.4305115e-06 -1.4305115e-06 3.8146973e-06 -1.6689301e-06 1.1920929e-07
		 6.6757202e-06 0 1.1920929e-07 7.6293945e-06 5.7220459e-06 1.1920929e-07 -6.6757202e-06
		 8.1062317e-06 1.1920929e-07 0 0 1.1920929e-07 -2.8610229e-06 0 1.4305115e-06 1.9073486e-06
		 -3.3378601e-06 1.4305115e-06 1.9073486e-06 9.5367432e-07 1.4305115e-06 -4.7683716e-07
		 -4.7683716e-07 1.4305115e-06 3.8146973e-06 -7.6293945e-06 1.4305115e-06 3.5762787e-06
		 -9.5367432e-07 -1.9073486e-06 2.3841858e-06 3.3378601e-06 1.4305115e-06 -8.3446503e-07
		 -4.2915344e-06 1.4305115e-06 -1.4901161e-07 -9.5367432e-07 -7.1525574e-07 3.8743019e-07
		 4.7683716e-07 1.4305115e-06 -2.2649765e-06 2.6226044e-06 3.3378601e-06 -2.6226044e-06
		 4.7683716e-07 1.4305115e-06 3.8146973e-06 8.3446503e-07 1.4305115e-06 4.7683716e-06
		 0 1.4305115e-06 6.1988831e-06 1.4305115e-06 1.4305115e-06 5.2452087e-06 -1.1920929e-06
		 2.1457672e-06 -4.2915344e-06 2.8610229e-06 1.4305115e-06 2.3841858e-06 5.7220459e-06
		 1.4305115e-06 6.6757202e-06 4.7683716e-06 1.4305115e-06 -2.8610229e-06 -8.1062317e-06
		 1.4305115e-06 -4.2915344e-06 0 1.4305115e-06 4.2915344e-06 2.3841858e-06 1.4305115e-06
		 7.6293945e-06 -6.6757202e-06 1.4305115e-06 1.9073486e-06 4.7683716e-07 1.4305115e-06
		 4.7683716e-07 2.3841858e-06 1.4305115e-06 -4.7683716e-07 5.7220459e-06 1.4305115e-06
		 4.7683716e-06 -1.4305115e-06 1.4305115e-06 3.5762787e-06 4.7683716e-07 1.4305115e-06
		 8.3446503e-07 4.2915344e-06 1.4305115e-06 -5.364418e-07 3.5762787e-06 2.8610229e-06
		 2.9802322e-08 4.7683716e-07 1.4305115e-06 7.1525574e-07 7.1525574e-07 3.3378601e-06
		 2.3841858e-07 -7.1525574e-07 1.4305115e-06 2.6226044e-06 -1.7881393e-07 1.4305115e-06
		 -3.3378601e-06 2.0861626e-07 1.4305115e-06 1.9073486e-06 -1.1920929e-06 1.4305115e-06
		 5.7220459e-06 4.529953e-06 -1.9073486e-06 -4.7683716e-07 2.8610229e-06 1.4305115e-06
		 -1.4305115e-06 -6.1988831e-06 1.4305115e-06 4.7683716e-06 -1.9073486e-06 1.4305115e-06
		 4.2915344e-06 -7.6293945e-06 1.4305115e-06 1.9073486e-06 0 1.4305115e-06 7.1525574e-06
		 -2.3841858e-06 -9.5367432e-07 -1.0490417e-05 -6.6757202e-06 -9.5367432e-07 1.9073486e-06
		 -2.3841858e-06 -9.5367432e-07 6.6757202e-06 3.8146973e-06 -9.5367432e-07 -2.8610229e-06
		 -4.2915344e-06 -9.5367432e-07 1.4305115e-06 -9.5367432e-07 1.1920929e-06 -1.4305115e-06
		 4.7683716e-07 -9.5367432e-07 8.3446503e-07 -3.3378601e-06 -9.5367432e-07 7.1525574e-07
		 2.1457672e-06 2.6226044e-06 2.9802322e-07 1.9073486e-06 -9.5367432e-07 -3.2782555e-07
		 4.7683716e-07 -2.8610229e-06 1.1920929e-06 -8.9406967e-08 -9.5367432e-07 1.4305115e-06
		 2.9802322e-08 -9.5367432e-07 2.6226044e-06 -1.1920929e-07 -9.5367432e-07 -4.0531158e-06
		 -1.4305115e-06 -9.5367432e-07 9.5367432e-07 -3.8146973e-06 -2.3841858e-06 -5.2452087e-06
		 -2.8610229e-06 -9.5367432e-07 6.6757202e-06 -3.8146973e-06 -9.5367432e-07 6.1988831e-06
		 6.6757202e-06 -9.5367432e-07 -8.1062317e-06 -7.6293945e-06 -9.5367432e-07 2.3841858e-06
		 0 -9.5367432e-07 -4.7683716e-06 1.9073486e-06 -4.7683716e-07 2.8610229e-06 2.3841858e-06
		 -4.7683716e-07 1.0490417e-05 2.3841858e-06 -4.7683716e-07 -9.5367432e-07 5.2452087e-06
		 -4.7683716e-07 4.2915344e-06 3.0994415e-06 -4.7683716e-07 -1.9073486e-06 -4.0531158e-06
		 2.3841858e-07 -1.9073486e-06 3.0994415e-06 -4.7683716e-07 2.3841858e-06;
	setAttr ".tk[2842:3007]" 1.1920929e-06 -4.7683716e-07 1.4305115e-06 -4.7683716e-07
		 4.7683716e-06 0 0 -4.7683716e-07 -3.2782555e-07 2.0861626e-07 -4.2915344e-06 -1.7881393e-07
		 5.6624413e-07 -4.7683716e-07 -3.5762787e-07 2.9802322e-07 -4.7683716e-07 -7.1525574e-07
		 -1.1920929e-07 -4.7683716e-07 -2.6226044e-06 1.1920929e-06 -4.7683716e-07 2.3841858e-06
		 -4.2915344e-06 -1.9073486e-06 1.4305115e-06 -4.7683716e-07 -4.7683716e-07 3.5762787e-06
		 -3.3378601e-06 -4.7683716e-07 1.9073486e-06 1.9073486e-06 -4.7683716e-07 1.4305115e-06
		 2.8610229e-06 -4.7683716e-07 -3.3378601e-06 0 -4.7683716e-07 -4.2915344e-06 0 -4.7683716e-07
		 -6.6757202e-06 -4.0531158e-06 -4.7683716e-07 9.5367432e-06 -1.9073486e-06 -4.7683716e-07
		 1.001358e-05 2.6226044e-06 -4.7683716e-07 -7.6293945e-06 1.4305115e-06 -4.7683716e-07
		 -2.8610229e-06 2.3841858e-07 1.4305115e-06 -2.3841858e-06 5.9604645e-07 -4.7683716e-07
		 -4.0531158e-06 -9.5367432e-07 -4.7683716e-07 9.5367432e-07 2.9802322e-07 2.6226044e-06
		 2.5033951e-06 1.8626451e-09 -4.7683716e-07 -8.3446503e-07 1.1920929e-07 -2.8610229e-06
		 -5.9604645e-08 1.1920929e-06 -4.7683716e-07 -1.4901161e-08 -5.9604645e-07 -4.7683716e-07
		 5.9604645e-07 1.1920929e-06 -4.7683716e-07 -2.2649765e-06 -3.0994415e-06 -4.7683716e-07
		 -1.5497208e-06 4.7683716e-07 -2.3841858e-06 3.5762787e-07 -1.9073486e-06 -4.7683716e-07
		 -1.1920929e-06 7.6293945e-06 -4.7683716e-07 -7.1525574e-07 -1.0490417e-05 -4.7683716e-07
		 1.6689301e-06 -1.0490417e-05 -4.7683716e-07 -1.6689301e-06 0 -4.7683716e-07 2.6226044e-06
		 5.9604645e-07 1.4305115e-06 -7.6293945e-06 3.5762787e-07 1.4305115e-06 1.0490417e-05
		 1.7881393e-06 1.4305115e-06 3.8146973e-06 -9.5367432e-07 1.4305115e-06 -6.1988831e-06
		 -1.1920929e-07 1.4305115e-06 7.1525574e-06 -1.1920929e-07 1.4305115e-06 -9.5367432e-07
		 1.7881393e-07 1.4305115e-06 1.9073486e-06 3.7252903e-08 1.4305115e-06 1.1920929e-06
		 7.4505806e-08 3.0994415e-06 7.1525574e-07 -1.0728836e-06 1.4305115e-06 9.5367432e-07
		 -1.1920929e-06 3.8146973e-06 1.0728836e-06 2.3841858e-07 1.4305115e-06 4.1723251e-07
		 1.9073486e-06 1.4305115e-06 -4.1723251e-07 1.6689301e-06 1.4305115e-06 1.4901161e-07
		 4.2915344e-06 1.4305115e-06 5.364418e-07 -3.8146973e-06 -1.4305115e-06 -4.1723251e-07
		 2.3841858e-06 1.4305115e-06 5.9604645e-07 1.4305115e-06 1.4305115e-06 -9.5367432e-07
		 -4.2915344e-06 1.4305115e-06 2.3841858e-07 6.6757202e-06 1.4305115e-06 2.1457672e-06
		 0 1.4305115e-06 1.0728836e-06 4.1723251e-07 1.6689301e-06 6.6757202e-06 -2.9802322e-07
		 1.6689301e-06 1.0490417e-05 -5.364418e-07 1.6689301e-06 -5.7220459e-06 2.3841858e-07
		 1.6689301e-06 -2.8610229e-06 -1.7881393e-07 1.6689301e-06 8.5830688e-06 5.364418e-07
		 -1.4305115e-06 -3.3378601e-06 2.3841858e-07 1.6689301e-06 2.3841858e-06 1.7881393e-07
		 1.6689301e-06 -1.9073486e-06 1.0728836e-06 -4.7683716e-07 -3.0994415e-06 -1.1920929e-06
		 1.6689301e-06 7.1525574e-07 -1.6689301e-06 3.8146973e-06 2.3841858e-07 2.6226044e-06
		 1.6689301e-06 8.3446503e-07 -2.8610229e-06 1.6689301e-06 -3.5762787e-07 -2.8610229e-06
		 1.6689301e-06 -1.1920929e-07 -1.9073486e-06 1.6689301e-06 -2.9802322e-08 -3.8146973e-06
		 2.6226044e-06 2.0861626e-07 4.7683716e-07 1.6689301e-06 -2.9802322e-07 7.1525574e-06
		 1.6689301e-06 0 0 1.6689301e-06 -1.4901161e-08 4.7683716e-06 1.6689301e-06 -4.7683716e-07
		 0 1.6689301e-06 3.5762787e-07 -1.4901161e-08 4.7683716e-07 -8.5830688e-06 1.1920929e-07
		 4.7683716e-07 1.0490417e-05 -2.3841858e-07 4.7683716e-07 -4.7683716e-06 8.9406967e-08
		 4.7683716e-07 6.1988831e-06 2.682209e-07 4.7683716e-07 4.7683716e-07 -1.4901161e-07
		 1.0728836e-06 2.8610229e-06 -2.0265579e-06 4.7683716e-07 -1.9073486e-06 7.1525574e-07
		 4.7683716e-07 -4.7683716e-07 1.1920929e-07 -2.3841858e-07 -3.8146973e-06 -9.5367432e-07
		 4.7683716e-07 -7.1525574e-07 -3.8146973e-06 -4.7683716e-07 1.4305115e-06 2.1457672e-06
		 4.7683716e-07 1.1920929e-06 -2.8610229e-06 4.7683716e-07 9.5367432e-07 6.1988831e-06
		 4.7683716e-07 -2.3841858e-07 1.9073486e-06 4.7683716e-07 -5.9604645e-08 -4.2915344e-06
		 -1.0728836e-06 -7.4505806e-08 -1.4305115e-06 4.7683716e-07 -1.4901161e-08 3.8146973e-06
		 4.7683716e-07 7.7486038e-07 -1.4305115e-06 4.7683716e-07 -2.3841858e-07 8.5830688e-06
		 4.7683716e-07 -1.4901161e-08 0 4.7683716e-07 0 0 6.2527761e-13 9.5367432e-06 -1.4901161e-08
		 6.2527761e-13 5.7220459e-06 5.9604645e-08 -8.3446503e-07 1.0490417e-05 -7.4505806e-08
		 -8.3446503e-07 -8.5830688e-06 3.7252903e-08 6.2527761e-13 -4.2915344e-06 2.0861626e-07
		 -8.3446503e-07 -5.2452087e-06 5.9604645e-08 6.2527761e-13 -2.3841858e-06 -1.7881393e-07
		 -8.3446503e-07 5.2452087e-06 4.7683716e-07 6.2527761e-13 -1.9073486e-06 -2.9802322e-08
		 -8.3446503e-07 -4.7683716e-07 -2.9802322e-08 -1.7053026e-13 -2.8610229e-06 -4.4703484e-08
		 -1.1920929e-06 2.3841858e-06 9.5367432e-07 6.2527761e-13 -1.4305115e-06 -1.3113022e-06
		 -8.3446503e-07 -2.8610229e-06 -1.3113022e-06 6.2527761e-13 -5.2452087e-06 -1.1920929e-07
		 -8.3446503e-07 -4.7683716e-07 -8.3446503e-07 -3.9790393e-13 7.1525574e-07 -9.5367432e-07
		 0 -5.0067902e-06 -1.4305115e-06 6.2527761e-13 1.4305115e-06 0 -8.3446503e-07 -1.9073486e-06
		 -2.3841858e-06 -1.080025e-12 2.1457672e-06 -2.3841858e-06 0 2.3841858e-07 3.5762787e-06
		 6.2527761e-13 4.7683716e-07 2.8610229e-06 -8.3446503e-07 7.1525574e-07 -9.5367432e-07
		 6.2527761e-13 1.9073486e-06 -1.9073486e-06 -8.3446503e-07 2.3841858e-06 2.8610229e-06
		 6.2527761e-13 3.5762787e-07 -9.5367432e-07 -8.3446503e-07 -1.1920929e-06 1.9073486e-06
		 6.2527761e-13 -1.1920929e-06 2.8610229e-06 -8.3446503e-07 5.9604645e-08 -9.5367432e-07
		 1.080025e-12 -2.0861626e-07 -3.3378601e-06 8.3446503e-07 5.9604645e-08 4.7683716e-07
		 6.2527761e-13 4.1723251e-07 -9.5367432e-07 -8.3446503e-07 -1.6391277e-07 8.5830688e-06
		 6.2527761e-13 9.5367432e-07 3.8146973e-06 -8.3446503e-07 -3.5762787e-07 1.0490417e-05
		 6.2527761e-13 4.7683716e-07 -9.5367432e-07 -8.3446503e-07 2.9802322e-07 -5.7220459e-06
		 6.2527761e-13 -3.7252903e-09 8.5830688e-06 -8.3446503e-07 5.9604645e-08 0 6.2527761e-13
		 2.7939677e-09 0 -8.3446503e-07 7.4505806e-08 -2.9802322e-07 -1.6689301e-06 1.0490417e-05
		 -4.1723251e-07 -1.6689301e-06 6.6757202e-06 1.013279e-06 -1.6689301e-06 -6.1988831e-06
		 -8.9406967e-08 -1.6689301e-06 -3.3378601e-06 5.9604645e-08 -1.6689301e-06 8.5830688e-06
		 7.1525574e-07 1.4305115e-06 -4.2915344e-06 -5.9604645e-07 -1.6689301e-06 1.4305115e-06
		 -7.1525574e-07 -1.6689301e-06 -2.8610229e-06 7.7486038e-07 0 -4.0531158e-06 -5.9604645e-07
		 -1.6689301e-06 1.4305115e-06 -1.9073486e-06 -4.0531158e-06 1.9073486e-06 1.9073486e-06
		 -1.6689301e-06 -8.3446503e-07 -1.9073486e-06 -1.6689301e-06 -1.1920929e-06 -1.9073486e-06
		 -1.6689301e-06 -9.5367432e-07 -1.4305115e-06 -1.6689301e-06 -8.9406967e-08 -3.3378601e-06
		 -2.8610229e-06 1.4901161e-08 1.4305115e-06 -1.6689301e-06 1.4901161e-07 7.6293945e-06
		 -1.6689301e-06 -7.4505806e-09 0 -1.6689301e-06 7.4505806e-08 4.7683716e-06 -1.6689301e-06
		 1.7881393e-07 0 -1.6689301e-06 -1.1920929e-07 1.1920929e-06 -1.4305115e-06 1.0490417e-05
		 5.9604645e-07 -1.4305115e-06 -7.6293945e-06 1.1920929e-07 -1.4305115e-06 3.3378601e-06
		 1.1920929e-07 -1.4305115e-06 -6.6757202e-06 -1.1920929e-07 -1.4305115e-06 6.6757202e-06;
	setAttr ".tk[3008:3173]" 4.7683716e-07 -1.6689301e-06 -1.4305115e-06 -2.682209e-07
		 -1.4305115e-06 1.4305115e-06 1.4901161e-08 -1.4305115e-06 4.7683716e-07 -5.2154064e-08
		 -3.0994415e-06 -2.3841858e-07 -4.1723251e-07 -1.4305115e-06 4.7683716e-07 -8.3446503e-07
		 -3.8146973e-06 2.3841858e-07 1.1920929e-06 -1.4305115e-06 -5.364418e-07 1.1920929e-06
		 -1.4305115e-06 -5.9604645e-07 1.9073486e-06 -1.4305115e-06 7.4505806e-08 4.7683716e-06
		 -1.4305115e-06 1.7881393e-07 -3.8146973e-06 1.6689301e-06 -9.5367432e-07 3.3378601e-06
		 -1.4305115e-06 -7.1525574e-07 -1.001358e-05 -1.4305115e-06 -1.3113022e-06 -4.2915344e-06
		 -1.4305115e-06 2.0265579e-06 6.6757202e-06 -1.4305115e-06 1.3113022e-06 0 -1.4305115e-06
		 2.3841858e-07 -3.8146973e-06 9.5367432e-07 9.5367432e-06 4.7683716e-07 9.5367432e-07
		 -6.6757202e-06 -1.6689301e-06 9.5367432e-07 9.5367432e-06 1.4305115e-06 9.5367432e-07
		 -8.1062317e-06 1.6689301e-06 9.5367432e-07 -2.8610229e-06 -2.6226044e-06 -1.1920929e-06
		 -2.3841858e-06 9.5367432e-07 9.5367432e-07 -4.0531158e-06 -9.5367432e-07 9.5367432e-07
		 4.7683716e-07 6.5565109e-07 -2.6226044e-06 2.0265579e-06 1.8626451e-09 9.5367432e-07
		 -1.0728836e-06 -1.0728836e-06 2.8610229e-06 0 1.4305115e-06 9.5367432e-07 4.4703484e-08
		 -9.5367432e-07 9.5367432e-07 1.1920929e-07 1.1920929e-06 9.5367432e-07 0 -2.8610229e-06
		 9.5367432e-07 -1.9073486e-06 4.7683716e-07 2.3841858e-06 -4.7683716e-07 -1.9073486e-06
		 9.5367432e-07 -1.6689301e-06 7.6293945e-06 9.5367432e-07 -1.4305115e-06 -1.0490417e-05
		 9.5367432e-07 2.6226044e-06 -1.0490417e-05 9.5367432e-07 -1.6689301e-06 0 9.5367432e-07
		 2.1457672e-06 2.3841858e-06 4.7683716e-07 1.0490417e-05 1.9073486e-06 4.7683716e-07
		 2.8610229e-06 2.3841858e-06 4.7683716e-07 -9.5367432e-07 5.2452087e-06 4.7683716e-07
		 4.2915344e-06 3.0994415e-06 4.7683716e-07 -1.9073486e-06 -4.0531158e-06 -2.3841858e-07
		 -1.9073486e-06 3.0994415e-06 4.7683716e-07 2.3841858e-06 1.1920929e-06 4.7683716e-07
		 1.4305115e-06 -4.7683716e-07 -4.7683716e-06 0 0 4.7683716e-07 -3.2782555e-07 2.0861626e-07
		 4.2915344e-06 -1.7881393e-07 5.6624413e-07 4.7683716e-07 -3.5762787e-07 2.9802322e-07
		 4.7683716e-07 -7.1525574e-07 -1.1920929e-07 4.7683716e-07 -2.6226044e-06 1.1920929e-06
		 4.7683716e-07 2.3841858e-06 -4.2915344e-06 1.9073486e-06 1.4305115e-06 -4.7683716e-07
		 4.7683716e-07 3.5762787e-06 -3.3378601e-06 4.7683716e-07 1.9073486e-06 1.9073486e-06
		 4.7683716e-07 1.4305115e-06 2.8610229e-06 4.7683716e-07 -3.3378601e-06 0 4.7683716e-07
		 -4.2915344e-06 -6.6757202e-06 4.7683716e-07 1.9073486e-06 -2.3841858e-06 4.7683716e-07
		 -1.0490417e-05 -2.3841858e-06 4.7683716e-07 6.6757202e-06 3.8146973e-06 4.7683716e-07
		 -2.8610229e-06 -4.2915344e-06 4.7683716e-07 1.4305115e-06 -9.5367432e-07 -1.4305115e-06
		 -1.4305115e-06 4.7683716e-07 4.7683716e-07 8.3446503e-07 -3.3378601e-06 4.7683716e-07
		 7.1525574e-07 2.1457672e-06 -2.6226044e-06 2.9802322e-07 1.9073486e-06 4.7683716e-07
		 -3.2782555e-07 4.7683716e-07 2.3841858e-06 1.1920929e-06 -8.9406967e-08 4.7683716e-07
		 1.4305115e-06 2.9802322e-08 4.7683716e-07 2.6226044e-06 -1.1920929e-07 4.7683716e-07
		 -4.0531158e-06 -1.4305115e-06 4.7683716e-07 9.5367432e-07 -3.8146973e-06 2.3841858e-06
		 -5.2452087e-06 -2.8610229e-06 4.7683716e-07 6.6757202e-06 -3.8146973e-06 4.7683716e-07
		 6.1988831e-06 6.6757202e-06 4.7683716e-07 -8.1062317e-06 -7.6293945e-06 4.7683716e-07
		 2.3841858e-06 0 4.7683716e-07 -4.7683716e-06 -4.7683716e-06 -1.4305115e-06 1.9073486e-06
		 1.4305115e-06 -1.4305115e-06 7.6293945e-06 -4.7683716e-07 -1.4305115e-06 4.7683716e-07
		 1.9073486e-06 -1.4305115e-06 -4.7683716e-06 5.2452087e-06 -1.4305115e-06 5.2452087e-06
		 -1.4305115e-06 -1.4305115e-06 3.8146973e-06 0 -1.4305115e-06 1.0728836e-06 4.2915344e-06
		 -1.4305115e-06 -3.5762787e-07 2.6226044e-06 -3.0994415e-06 1.4901161e-08 4.7683716e-07
		 -1.4305115e-06 9.5367432e-07 -4.7683716e-07 -3.3378601e-06 2.3841858e-07 -7.1525574e-07
		 -1.4305115e-06 3.3378601e-06 -1.7881393e-07 -1.4305115e-06 -2.8610229e-06 4.7683716e-07
		 -1.4305115e-06 2.3841858e-06 -1.5497208e-06 -1.4305115e-06 6.6757202e-06 4.2915344e-06
		 1.4305115e-06 4.7683716e-07 2.8610229e-06 -1.4305115e-06 -1.4305115e-06 -6.6757202e-06
		 -1.4305115e-06 5.2452087e-06 -3.3378601e-06 -1.4305115e-06 4.7683716e-06 -7.6293945e-06
		 -1.4305115e-06 2.3841858e-06 0 -1.4305115e-06 7.1525574e-06 -3.3378601e-06 -1.6689301e-06
		 1.9073486e-06 0 -1.6689301e-06 1.9073486e-06 9.5367432e-07 -1.6689301e-06 -4.7683716e-07
		 -4.7683716e-07 -1.6689301e-06 3.8146973e-06 -7.6293945e-06 -1.6689301e-06 3.5762787e-06
		 -9.5367432e-07 1.1920929e-06 2.3841858e-06 3.3378601e-06 -1.6689301e-06 -8.3446503e-07
		 -4.2915344e-06 -1.6689301e-06 -1.4901161e-07 -9.5367432e-07 0 3.8743019e-07 4.7683716e-07
		 -1.6689301e-06 -2.2649765e-06 2.6226044e-06 -4.0531158e-06 -2.6226044e-06 4.7683716e-07
		 -1.6689301e-06 3.8146973e-06 8.3446503e-07 -1.6689301e-06 4.7683716e-06 0 -1.6689301e-06
		 6.1988831e-06 1.4305115e-06 -1.6689301e-06 5.2452087e-06 -1.1920929e-06 -3.0994415e-06
		 -4.2915344e-06 2.8610229e-06 -1.6689301e-06 2.3841858e-06 5.7220459e-06 -1.6689301e-06
		 6.6757202e-06 4.7683716e-06 -1.6689301e-06 -2.8610229e-06 -8.1062317e-06 -1.6689301e-06
		 -4.2915344e-06 0 -1.6689301e-06 4.2915344e-06 5.7220459e-06 -9.5367432e-07 2.8610229e-06
		 -1.9073486e-06 -9.5367432e-07 3.8146973e-06 9.5367432e-06 -9.5367432e-07 -2.3841858e-06
		 2.8610229e-06 -9.5367432e-07 -3.3378601e-06 -2.3841858e-06 -9.5367432e-07 -7.1525574e-07
		 3.3378601e-06 -1.1920929e-07 1.6689301e-06 -1.9073486e-06 -9.5367432e-07 1.013279e-06
		 -1.4305115e-06 -9.5367432e-07 2.682209e-07 -1.9073486e-06 -1.1920929e-07 4.1723251e-07
		 0 -9.5367432e-07 -1.1920929e-06 -4.0531158e-06 -2.3841858e-07 5.0067902e-06 1.4305115e-06
		 -9.5367432e-07 -4.7683716e-07 1.6689301e-06 -9.5367432e-07 -4.2915344e-06 -5.9604645e-07
		 -9.5367432e-07 -3.3378601e-06 8.3446503e-07 -9.5367432e-07 9.5367432e-06 -1.4305115e-06
		 7.1525574e-07 3.8146973e-06 -1.6689301e-06 -9.5367432e-07 6.6757202e-06 0 -9.5367432e-07
		 7.6293945e-06 5.7220459e-06 -9.5367432e-07 -6.6757202e-06 8.1062317e-06 -9.5367432e-07
		 0 0 -9.5367432e-07 -2.8610229e-06 -9.5367432e-06 6.2527761e-13 2.8610229e-06 -1.9073486e-06
		 6.2527761e-13 -8.5830688e-06 0 6.2527761e-13 -3.3378601e-06 -4.2915344e-06 6.2527761e-13
		 3.8146973e-06 3.3378601e-06 6.2527761e-13 7.1525574e-07 3.3378601e-06 -1.7053026e-13
		 -2.1457672e-06 3.3378601e-06 6.2527761e-13 5.364418e-07 -7.1525574e-06 6.2527761e-13
		 1.7881393e-07 1.9073486e-06 -3.9790393e-13 -3.5762787e-07 4.7683716e-06 6.2527761e-13
		 -1.1920929e-06 9.5367432e-07 -1.080025e-12 0 -1.4305115e-06 6.2527761e-13 -2.8610229e-06
		 -1.6689301e-06 6.2527761e-13 4.2915344e-06 4.7683716e-07 6.2527761e-13 3.3378601e-06
		 3.5762787e-07 6.2527761e-13 4.7683716e-06 -2.8610229e-06 1.080025e-12 1.9073486e-06
		 -3.0994415e-06 6.2527761e-13 -8.5830688e-06 4.7683716e-06 6.2527761e-13 -2.3841858e-06
		 6.1988831e-06 6.2527761e-13 -2.8610229e-06 -8.5830688e-06 6.2527761e-13 -2.8610229e-06
		 0 6.2527761e-13 -9.5367432e-07 5.7220459e-06 1.1920929e-07 2.8610229e-06 -1.9073486e-06
		 1.1920929e-07 3.8146973e-06 9.5367432e-06 1.1920929e-07 -2.3841858e-06;
	setAttr ".tk[3174:3339]" 2.8610229e-06 1.1920929e-07 -3.3378601e-06 -2.3841858e-06
		 1.1920929e-07 -7.1525574e-07 3.3378601e-06 7.1525574e-07 1.6689301e-06 -1.9073486e-06
		 1.1920929e-07 1.013279e-06 -1.4305115e-06 1.1920929e-07 2.682209e-07 -1.9073486e-06
		 -5.9604645e-07 4.1723251e-07 0 1.1920929e-07 -1.1920929e-06 -4.0531158e-06 -7.1525574e-07
		 5.0067902e-06 1.4305115e-06 1.1920929e-07 -4.7683716e-07 1.6689301e-06 1.1920929e-07
		 -4.2915344e-06 -5.9604645e-07 1.1920929e-07 -3.3378601e-06 8.3446503e-07 1.1920929e-07
		 9.5367432e-06 -1.4305115e-06 -1.4305115e-06 3.8146973e-06 -1.6689301e-06 1.1920929e-07
		 6.6757202e-06 0 1.1920929e-07 7.6293945e-06 5.7220459e-06 1.1920929e-07 -6.6757202e-06
		 8.1062317e-06 1.1920929e-07 0 0 1.1920929e-07 -2.8610229e-06 -3.3378601e-06 1.4305115e-06
		 1.9073486e-06 0 1.4305115e-06 1.9073486e-06 9.5367432e-07 1.4305115e-06 -4.7683716e-07
		 -4.7683716e-07 1.4305115e-06 3.8146973e-06 -7.6293945e-06 1.4305115e-06 3.5762787e-06
		 -9.5367432e-07 -1.9073486e-06 2.3841858e-06 3.3378601e-06 1.4305115e-06 -8.3446503e-07
		 -4.2915344e-06 1.4305115e-06 -1.4901161e-07 -9.5367432e-07 -7.1525574e-07 3.8743019e-07
		 4.7683716e-07 1.4305115e-06 -2.2649765e-06 2.6226044e-06 3.3378601e-06 -2.6226044e-06
		 4.7683716e-07 1.4305115e-06 3.8146973e-06 8.3446503e-07 1.4305115e-06 4.7683716e-06
		 0 1.4305115e-06 6.1988831e-06 1.4305115e-06 1.4305115e-06 5.2452087e-06 -1.1920929e-06
		 2.1457672e-06 -4.2915344e-06 2.8610229e-06 1.4305115e-06 2.3841858e-06 5.7220459e-06
		 1.4305115e-06 6.6757202e-06 4.7683716e-06 1.4305115e-06 -2.8610229e-06 -8.1062317e-06
		 1.4305115e-06 -4.2915344e-06 0 1.4305115e-06 4.2915344e-06 -6.6757202e-06 1.4305115e-06
		 1.9073486e-06 2.3841858e-06 1.4305115e-06 7.6293945e-06 4.7683716e-07 1.4305115e-06
		 4.7683716e-07 2.3841858e-06 1.4305115e-06 -4.7683716e-07 5.7220459e-06 1.4305115e-06
		 4.7683716e-06 -1.4305115e-06 1.4305115e-06 3.5762787e-06 4.7683716e-07 1.4305115e-06
		 8.3446503e-07 4.2915344e-06 1.4305115e-06 -5.364418e-07 3.5762787e-06 2.8610229e-06
		 2.9802322e-08 4.7683716e-07 1.4305115e-06 7.1525574e-07 7.1525574e-07 3.3378601e-06
		 2.3841858e-07 -7.1525574e-07 1.4305115e-06 2.6226044e-06 -1.7881393e-07 1.4305115e-06
		 -3.3378601e-06 2.0861626e-07 1.4305115e-06 1.9073486e-06 -1.1920929e-06 1.4305115e-06
		 5.7220459e-06 4.529953e-06 -1.9073486e-06 -4.7683716e-07 2.8610229e-06 1.4305115e-06
		 -1.4305115e-06 -6.1988831e-06 1.4305115e-06 4.7683716e-06 -1.9073486e-06 1.4305115e-06
		 4.2915344e-06 -7.6293945e-06 1.4305115e-06 1.9073486e-06 0 1.4305115e-06 7.1525574e-06
		 -6.6757202e-06 -9.5367432e-07 1.9073486e-06 -2.3841858e-06 -9.5367432e-07 -1.0490417e-05
		 -2.3841858e-06 -9.5367432e-07 6.6757202e-06 3.8146973e-06 -9.5367432e-07 -2.8610229e-06
		 -4.2915344e-06 -9.5367432e-07 1.4305115e-06 -9.5367432e-07 1.1920929e-06 -1.4305115e-06
		 4.7683716e-07 -9.5367432e-07 8.3446503e-07 -3.3378601e-06 -9.5367432e-07 7.1525574e-07
		 2.1457672e-06 2.6226044e-06 2.9802322e-07 1.9073486e-06 -9.5367432e-07 -3.2782555e-07
		 4.7683716e-07 -2.8610229e-06 1.1920929e-06 -8.9406967e-08 -9.5367432e-07 1.4305115e-06
		 2.9802322e-08 -9.5367432e-07 2.6226044e-06 -1.1920929e-07 -9.5367432e-07 -4.0531158e-06
		 -1.4305115e-06 -9.5367432e-07 9.5367432e-07 -3.8146973e-06 -2.3841858e-06 -5.2452087e-06
		 -2.8610229e-06 -9.5367432e-07 6.6757202e-06 -3.8146973e-06 -9.5367432e-07 6.1988831e-06
		 6.6757202e-06 -9.5367432e-07 -8.1062317e-06 -7.6293945e-06 -9.5367432e-07 2.3841858e-06
		 0 -9.5367432e-07 -4.7683716e-06 2.3841858e-06 -4.7683716e-07 1.0490417e-05 1.9073486e-06
		 -4.7683716e-07 2.8610229e-06 2.3841858e-06 -4.7683716e-07 -9.5367432e-07 5.2452087e-06
		 -4.7683716e-07 4.2915344e-06 3.0994415e-06 -4.7683716e-07 -1.9073486e-06 -4.0531158e-06
		 2.3841858e-07 -1.9073486e-06 3.0994415e-06 -4.7683716e-07 2.3841858e-06 1.1920929e-06
		 -4.7683716e-07 1.4305115e-06 -4.7683716e-07 4.7683716e-06 0 0 -4.7683716e-07 -3.2782555e-07
		 2.0861626e-07 -4.2915344e-06 -1.7881393e-07 5.6624413e-07 -4.7683716e-07 -3.5762787e-07
		 2.9802322e-07 -4.7683716e-07 -7.1525574e-07 -1.1920929e-07 -4.7683716e-07 -2.6226044e-06
		 1.1920929e-06 -4.7683716e-07 2.3841858e-06 -4.2915344e-06 -1.9073486e-06 1.4305115e-06
		 -4.7683716e-07 -4.7683716e-07 3.5762787e-06 -3.3378601e-06 -4.7683716e-07 1.9073486e-06
		 1.9073486e-06 -4.7683716e-07 1.4305115e-06 2.8610229e-06 -4.7683716e-07 -3.3378601e-06
		 0 -4.7683716e-07 -4.2915344e-06 -4.0531158e-06 -4.7683716e-07 9.5367432e-06 0 -4.7683716e-07
		 -6.6757202e-06 -1.9073486e-06 -4.7683716e-07 1.001358e-05 2.6226044e-06 -4.7683716e-07
		 -7.6293945e-06 1.4305115e-06 -4.7683716e-07 -2.8610229e-06 2.3841858e-07 1.4305115e-06
		 -2.3841858e-06 5.9604645e-07 -4.7683716e-07 -4.0531158e-06 -9.5367432e-07 -4.7683716e-07
		 9.5367432e-07 2.9802322e-07 2.6226044e-06 2.5033951e-06 1.8626451e-09 -4.7683716e-07
		 -8.3446503e-07 1.1920929e-07 -2.8610229e-06 -5.9604645e-08 1.1920929e-06 -4.7683716e-07
		 -1.4901161e-08 -5.9604645e-07 -4.7683716e-07 5.9604645e-07 1.1920929e-06 -4.7683716e-07
		 -2.2649765e-06 -3.0994415e-06 -4.7683716e-07 -1.5497208e-06 4.7683716e-07 -2.3841858e-06
		 3.5762787e-07 -1.9073486e-06 -4.7683716e-07 -1.1920929e-06 7.6293945e-06 -4.7683716e-07
		 -7.1525574e-07 -1.0490417e-05 -4.7683716e-07 1.6689301e-06 -1.0490417e-05 -4.7683716e-07
		 -1.6689301e-06 0 -4.7683716e-07 2.6226044e-06 3.5762787e-07 1.4305115e-06 1.0490417e-05
		 5.9604645e-07 1.4305115e-06 -7.6293945e-06 1.7881393e-06 1.4305115e-06 3.8146973e-06
		 -9.5367432e-07 1.4305115e-06 -6.1988831e-06 -1.1920929e-07 1.4305115e-06 7.1525574e-06
		 -1.1920929e-07 1.4305115e-06 -9.5367432e-07 1.7881393e-07 1.4305115e-06 1.9073486e-06
		 3.7252903e-08 1.4305115e-06 1.1920929e-06 7.4505806e-08 3.0994415e-06 7.1525574e-07
		 -1.0728836e-06 1.4305115e-06 9.5367432e-07 -1.1920929e-06 3.8146973e-06 1.0728836e-06
		 2.3841858e-07 1.4305115e-06 4.1723251e-07 1.9073486e-06 1.4305115e-06 -4.1723251e-07
		 1.6689301e-06 1.4305115e-06 1.4901161e-07 4.2915344e-06 1.4305115e-06 5.364418e-07
		 -3.8146973e-06 -1.4305115e-06 -4.1723251e-07 2.3841858e-06 1.4305115e-06 5.9604645e-07
		 1.4305115e-06 1.4305115e-06 -9.5367432e-07 -4.2915344e-06 1.4305115e-06 2.3841858e-07
		 6.6757202e-06 1.4305115e-06 2.1457672e-06 0 1.4305115e-06 1.0728836e-06 -2.9802322e-07
		 1.6689301e-06 1.0490417e-05 4.1723251e-07 1.6689301e-06 6.6757202e-06 -5.364418e-07
		 1.6689301e-06 -5.7220459e-06 2.3841858e-07 1.6689301e-06 -2.8610229e-06 -1.7881393e-07
		 1.6689301e-06 8.5830688e-06 5.364418e-07 -1.4305115e-06 -3.3378601e-06 2.3841858e-07
		 1.6689301e-06 2.3841858e-06 1.7881393e-07 1.6689301e-06 -1.9073486e-06 1.0728836e-06
		 -4.7683716e-07 -3.0994415e-06 -1.1920929e-06 1.6689301e-06 7.1525574e-07 -1.6689301e-06
		 3.8146973e-06 2.3841858e-07 2.6226044e-06 1.6689301e-06 8.3446503e-07 -2.8610229e-06
		 1.6689301e-06 -3.5762787e-07 -2.8610229e-06 1.6689301e-06 -1.1920929e-07 -1.9073486e-06
		 1.6689301e-06 -2.9802322e-08 -3.8146973e-06 2.6226044e-06 2.0861626e-07 4.7683716e-07
		 1.6689301e-06 -2.9802322e-07 7.1525574e-06 1.6689301e-06 0 0 1.6689301e-06 -1.4901161e-08
		 4.7683716e-06 1.6689301e-06 -4.7683716e-07 0 1.6689301e-06 3.5762787e-07 1.1920929e-07
		 4.7683716e-07 1.0490417e-05;
	setAttr ".tk[3340:3359]" -1.4901161e-08 4.7683716e-07 -8.5830688e-06 -2.3841858e-07
		 4.7683716e-07 -4.7683716e-06 8.9406967e-08 4.7683716e-07 6.1988831e-06 2.682209e-07
		 4.7683716e-07 4.7683716e-07 -1.4901161e-07 1.0728836e-06 2.8610229e-06 -2.0265579e-06
		 4.7683716e-07 -1.9073486e-06 7.1525574e-07 4.7683716e-07 -4.7683716e-07 1.1920929e-07
		 -2.3841858e-07 -3.8146973e-06 -9.5367432e-07 4.7683716e-07 -7.1525574e-07 -3.8146973e-06
		 -4.7683716e-07 1.4305115e-06 2.1457672e-06 4.7683716e-07 1.1920929e-06 -2.8610229e-06
		 4.7683716e-07 9.5367432e-07 6.1988831e-06 4.7683716e-07 -2.3841858e-07 1.9073486e-06
		 4.7683716e-07 -5.9604645e-08 -4.2915344e-06 -1.0728836e-06 -7.4505806e-08 -1.4305115e-06
		 4.7683716e-07 -1.4901161e-08 3.8146973e-06 4.7683716e-07 7.7486038e-07 -1.4305115e-06
		 4.7683716e-07 -2.3841858e-07 8.5830688e-06 4.7683716e-07 -1.4901161e-08 0 4.7683716e-07
		 0;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyExtrudeFace4.out" "pTorusShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "polyTorus1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polyExtrudeFace1.ip";
connectAttr "pTorusShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak1.out" "polyExtrudeFace2.ip";
connectAttr "pTorusShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak1.ip";
connectAttr "polyExtrudeFace2.out" "polyExtrudeFace3.ip";
connectAttr "pTorusShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyTweak2.out" "polyExtrudeFace4.ip";
connectAttr "pTorusShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak2.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pTorusShape1.iog" ":initialShadingGroup.dsm" -na;
// End of ggj_circ_tube.ma
