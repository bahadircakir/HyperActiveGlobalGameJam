﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    public VariableJoystick variableJoystick;

    public void FixedUpdate()
    {
        //Vector3 scaledMovement = speed * Time.deltaTime * new Vector3(MovementAmount.x,MovementAmount.y,0);
        var zVector = Vector3.forward * speed * Time.deltaTime;
        //var camZVector = new Vector3(0f,0f,1f) * Speed * Time.deltaTime;
        //transform.LookAt(Player.transform.position + scaledMovement, Vector3.up);
        //transform.position = scaledMovementpe
        transform.Translate(zVector*-1);
    }
}